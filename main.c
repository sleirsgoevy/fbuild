#if 0
if [[ ! -f "$0.bin" ]] || [[ "$0" -nt "$0.bin" ]]
then
    echo + cc -DSTAGE1 -x c "$0" -o "$0.bin"
    cc -DSTAGE1 -x c "$0" -o "$0.bin" || exit $?
fi
exec "$0.bin" "$@"
#endif

/*
    "fbuild" program build system
    Copyright (C) 2022  Sergey Lisov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>
#include <setjmp.h>

struct AsyncFrame
{
    struct AsyncFrame* next;
    struct AsyncFrame*(*fn)(struct AsyncFrame*);
    int state;
};

void AsyncCancel(struct AsyncFrame* frame)
{
    while(frame)
    {
        struct AsyncFrame* next = frame->next;
        free(frame);
        frame = next;
    }
}

struct AsyncFrame* AsyncReturn(struct AsyncFrame* frame)
{
    struct AsyncFrame* ans = frame->next;
    free(frame);
    return ans;
}

#define ASYNC_DECLARE(fn, args)\
struct AsyncFrameArgsOnly_ ## fn\
{\
    struct AsyncFrame Self;\
    args;\
};\
struct AsyncFrame* fn(struct AsyncFrame*, const void*, size_t);

#define ASYNC_TYPEDEF(fn, args)\
struct AsyncFrameArgsOnly_ ## fn\
{\
    struct AsyncFrame Self;\
    args;\
};\
typedef struct AsyncFrame* fn(struct AsyncFrame*, const void*, size_t);

#define ASYNC_DEFINE(fname, args, vars)\
struct AsyncFrame_ ## fname\
{\
    struct AsyncFrame Self;\
    args;\
    vars;\
};\
struct AsyncFrame* AsyncImpl_ ## fname(struct AsyncFrame*);\
struct AsyncFrame* fname(struct AsyncFrame* parent, const void* argp, size_t size)\
{\
    struct AsyncFrame_ ## fname* frame = malloc(sizeof(*frame));\
    memset(frame, 0, sizeof(*frame));\
    memcpy(frame, argp, size);\
    frame->Self.next = parent;\
    frame->Self.fn = AsyncImpl_ ## fname;\
    frame->Self.state = 0;\
    return (struct AsyncFrame*)frame;\
}\
struct AsyncFrame* AsyncImpl_ ## fname(struct AsyncFrame* Self)\
{\
    struct AsyncFrame_ ## fname* self = (void*)Self;\
    switch(self->Self.state)\
    {\
    case 0:

#define ASYNC_END()\
        return AsyncReturn(Self);\
    }\
}

#define FRAME_FN(fn, ...) fn(0, &(struct AsyncFrameArgsOnly_ ## fn){{0}, __VA_ARGS__}, sizeof(struct AsyncFrameArgsOnly_ ## fn))
#define FRAME_PTR(fn, ptr, ...) ptr(0, &(struct AsyncFrameArgsOnly_ ## fn){{0}, __VA_ARGS__}, sizeof(struct AsyncFrameArgsOnly_ ## fn))

#define AWAIT_FN(fn, ...) do\
{\
    self->Self.state = __COUNTER__ + 1;\
    return fn(Self, &(struct AsyncFrameArgsOnly_ ## fn){{0}, __VA_ARGS__}, sizeof(struct AsyncFrameArgsOnly_ ## fn));\
case __COUNTER__:\
    if(0);\
}\
while(0)

#define AWAIT_PTR(fn, ptr, ...) do\
{\
    self->Self.state = __COUNTER__ + 1;\
    return ptr(Self, &(struct AsyncFrameArgsOnly_ ## fn){{0}, __VA_ARGS__}, sizeof(struct AsyncFrameArgsOnly_ ## fn));\
case __COUNTER__:\
    if(0);\
}\
while(0)

#define RETURN() return AsyncReturn(Self)
#define TAILCALL_FN(fn, ...) return fn(AsyncReturn(Self), &(struct AsyncFrameArgsOnly_ ## fn){{0}, __VA_ARGS__}, sizeof(struct AsyncFrameArgsOnly_ ## fn))
#define TAILCALL_PTR(fn, ptr, ...) return ptr(AsyncReturn(Self), &(struct AsyncFrameArgsOnly_ ## fn){{0}, __VA_ARGS__}, sizeof(struct AsyncFrameArgsOnly_ ## fn))

ASYNC_DECLARE(AsyncYield, int request; void* payload)
ASYNC_DEFINE(AsyncYield, int request; void* payload,) ASYNC_END()

struct AsyncFrame* AsyncRunFrame(struct AsyncFrame* frame, int* request, void** payload)
{
    while(frame && frame->fn != AsyncImpl_AsyncYield)
        frame = frame->fn(frame);
    if(frame)
    {
        struct AsyncFrame_AsyncYield* p = (void*)frame;
        *request = p->request;
        *payload = p->payload;
        return AsyncReturn(frame);
    }
}

jmp_buf* error_exit_handler = NULL;

void error_exit(void)
{
    if(error_exit_handler)
        longjmp(*error_exit_handler, 1);
    exit(1);
}

int flush_caches(size_t target);

static size_t total_alloc = 0;
static size_t alloc_cap = 16;

#define ALLOC_CAP (cle ? (alloc_cap * alloc_cap * alloc_cap * alloc_cap * alloc_cap) : (alloc_cap * alloc_cap * alloc_cap * alloc_cap))

static inline void* chk_malloc_mbcle(size_t sz, int cle)
{
    while(total_alloc + sz > ALLOC_CAP && flush_caches(ALLOC_CAP - sz))
        alloc_cap *= 2;
    void* ans = malloc(sz);
    total_alloc += sz;
    if(!ans)
    {
        fprintf(stderr, "error: not enough memory\n");
        error_exit();
    }
    return ans;
}

static inline void* chk_malloc(size_t sz)
{
    return chk_malloc_mbcle(sz, 0);
}

static inline void* chk_calloc(size_t sz)
{
    void* ans = chk_malloc(sz);
    memset(ans, 0, sz);
    return ans;
}

static inline void* chk_realloc_mbcle(void* obj, size_t old_sz, size_t new_sz, int cle)
{
    while(total_alloc - old_sz + new_sz > ALLOC_CAP && flush_caches(ALLOC_CAP + old_sz - new_sz))
        alloc_cap *= 2;
    void* ans = realloc(obj, new_sz);
    total_alloc -= old_sz;
    total_alloc += new_sz;
    if(!ans)
    {
        fprintf(stderr, "error: not enough memory\n");
        error_exit();
    }
    return ans;
}

static inline void* chk_realloc(void* obj, size_t old_sz, size_t new_sz)
{
    return chk_realloc_mbcle(obj, old_sz, new_sz, 0);
}

static inline void* chk_free(void* obj, size_t sz)
{
    free(obj);
    total_alloc -= sz;
}

#undef ALLOC_CAP

static inline void* malloc_with_data(size_t sz, const void* data)
{
    void* ans = chk_malloc(sz);
    memcpy(ans, data, sz);
    return ans;
}

#define NEW(which, ...) malloc_with_data(sizeof(which), &(which)__VA_ARGS__)
#define NEWARR(which, var, i, n, val) which* var = chk_malloc(sizeof(*var) * (n)); for(int i = 0; i < n; i++) var[i] = val;

typedef struct
{
    enum { NONE, INT, STRING } which;
    union
    {
        const char* value_s;
        int value_i;
    };
} str_or_int;

#define MKNONE() (str_or_int){ .which = NONE, .value_s = NULL }
#define MKSTR(s) (str_or_int){ .which = STRING, .value_s = s }
#define MKINT(i) (str_or_int){ .which = INT, .value_i = i }

static inline int si_hash(str_or_int si)
{
    if(si.which == NONE)
        return 0xdeadbef;
    else if(si.which == INT)
        return si.value_i ^ 0x147147;
    else
    {
        long long hsh = 0x571571;
        for(int i = 0; si.value_s[i]; i++)
            hsh = (hsh * 256 + si.value_s[i]) % 0xdead571;
        return hsh ^ 0x123456;
    }
}

static inline int sicmp(str_or_int a, str_or_int b)
{
    if(a.which == NONE || b.which == NONE)
        return a.which != NONE || b.which != NONE;
    else if(a.which == INT || b.which == INT)
        return a.which != INT || b.which != INT || a.value_i != b.value_i;
    else
        return strcmp(a.value_s, b.value_s);
}

struct object;
typedef struct object object;

struct hash_bucket
{
    str_or_int si;
    void* value;
    struct hash_bucket* next;
};

struct hash_table
{
    struct hash_bucket** buckets;
    int size;
    int capacity;
};

struct cache_list_entry
{
    struct cache_list_entry* next;
    struct cache_list_entry** prev;
    struct hash_table* ht;
};

struct cache_list_entry* cache_list;
struct cache_list_entry* cache_list_end;

void free_bucket(struct hash_bucket* hb)
{
    while(hb)
    {
        struct hash_bucket* next = hb->next;
        chk_free(hb, sizeof(*hb));
        hb = next;
    }
}

void free_buckets(struct hash_bucket** hb, int cnt)
{
    for(int i = 0; i < cnt; i++)
        free_bucket(hb[i]);
    chk_free(hb, sizeof(*hb) * cnt);
}

int flush_caches(size_t target_cons)
{
    if(!cache_list_end)
        return -1;
    struct cache_list_entry** cur = &cache_list_end->next;
    while(total_alloc > target_cons && cur != &cache_list)
    {
        struct cache_list_entry* i = (struct cache_list_entry*)cur;
        cur = i->prev;
        free_buckets(i->ht->buckets, i->ht->capacity);
        i->ht->buckets = NULL;
        i->ht->size = i->ht->capacity = 0;
    }
    if(total_alloc > target_cons)
        return -1;
    return 0;
}

static struct hash_bucket** rehash(struct hash_bucket*** pold, int oldsz, int newsz, int cle)
{
    struct hash_bucket** neww = chk_malloc_mbcle(sizeof(*neww) * newsz, cle);
    struct hash_bucket** old = *pold;
    if(!old)
        oldsz = 0;
    memset(neww, 0, sizeof(*neww) * newsz);
    for(int i = 0; i < oldsz; i++)
        while(old[i])
        {
            struct hash_bucket* cur = old[i];
            old[i] = cur->next;
            int key = (unsigned)si_hash(cur->si) % (unsigned)newsz;
            cur->next = neww[key];
            neww[key] = cur;
        }
    free_buckets(old, oldsz);
    return neww;
}

void hash_table_add_mbcle(struct hash_table* ht, str_or_int key, void* value, int cle)
{
    unsigned hash0 = si_hash(key);
    if(ht->capacity)
    {
        int hash = hash0 % (unsigned)ht->capacity;
        for(struct hash_bucket* hb2 = ht->buckets[hash]; hb2; hb2 = hb2->next)
            if(!sicmp(hb2->si, key))
            {
                hb2->value = value;
                return;
            }
    }
    struct hash_bucket* hb = chk_malloc_mbcle(sizeof(struct hash_bucket), cle);
    *hb = (struct hash_bucket){key, value};
    if(ht->size >= 2 * ht->capacity)
    {
        int newsz = ht->capacity * 2;
        if(!newsz)
            newsz = 16;
        ht->buckets = rehash(&ht->buckets, ht->capacity, newsz, cle);
        ht->capacity = newsz;
    }
    int hash = hash0 % (unsigned)ht->capacity;
    hb->next = ht->buckets[hash];
    ht->buckets[hash] = hb;
    ht->size++;
}

void hash_table_add(struct hash_table* ht, str_or_int key, void* value)
{
    return hash_table_add_mbcle(ht, key, value, 0);
}

void* hash_table_get(const struct hash_table* ht, str_or_int key)
{
    if(ht->capacity == 0)
        return NULL;
    int hash = (unsigned)si_hash(key) % (unsigned)ht->capacity;
    for(const struct hash_bucket* hb = ht->buckets[hash]; hb; hb = hb->next)
        if(!sicmp(hb->si, key))
            return hb->value;
    return NULL;
}

const char* get_static_str(const char* s)
{
    static struct hash_table ht;
    const char* ans = hash_table_get(&ht, MKSTR(s));
    if(!ans)
    {
        int l = strlen(s);
        char* tmp = chk_malloc(l+1);
        memcpy(tmp, s, l+1);
        ans = tmp;
        hash_table_add(&ht, MKSTR(ans), (void*)ans);
    }
    return ans;
}

/* source: wikipedia */

struct sha256
{
    uint32_t h[8];
    const uint32_t* k;
    uint8_t bytes[64];
    int idx;
    uint64_t total_bits;
};

static inline int sha256_next_prime(int q)
{
    for(;;)
    {
        q++;
        int ok = 1;
        for(int i = 2; i * i <= q; i++)
            if(q % i == 0)
            {
                ok = 0;
                break;
            }
        if(ok)
            return q;
    }
}

static inline void sha256_longmult(uint32_t c[4], const uint32_t a[4], const uint32_t b[4])
{
    for(int i = 0; i < 4; i++)
        c[i] = 0;
    for(int i = 0; i < 4; i++)
        for(int j = 0; i + j < 4; j++)
        {
            uint64_t prod = (uint64_t)a[i] * (uint64_t)b[j];
            for(int k = i + j; k < 4; k++)
            {
                prod += c[k];
                c[k] = prod;
                prod >>= 32;
            }
        }
}

static inline uint32_t sha256_intqrt(int num, int cubic)
{
    int low = 1;
    int high = num;
    while(high - low > 1)
    {
        int mid = (high + low) / 2;
        int mm = mid * mid;
        if(cubic)
            mm *= mid;
        if(mm > num)
            high = mid;
        else
            low = mid;
    }
    return low;
}

static inline uint32_t sha256_qrt(int num, int cubic)
{
    uint32_t isqrt = sha256_intqrt(num, cubic);
    uint32_t low = 0;
    uint32_t high = -1;
    while(high - low > 0)
    {
        uint32_t mid = high - (high - low) / 2;
        uint32_t a[4] = {mid, isqrt, 0, 0}, b[4] = {mid, isqrt, 0, 0}, c[4];
        sha256_longmult(c, a, b);
        if(cubic)
            sha256_longmult(a, c, b);
        if((cubic ? a[3] : c[2]) >= num)
            high = mid - 1;
        else
            low = mid;
    }
    return low;
}

void sha256_init(struct sha256* self)
{
    memset(self, 0, sizeof(*self));
    static uint32_t h[8], k[64];
    if(!h[0])
    {
        int q = 1;
        for(int i = 0; i < 8; i++)
            h[i] = sha256_qrt(q = sha256_next_prime(q), 0);
        q = 1;
        for(int i = 0; i < 64; i++)
            k[i] = sha256_qrt(q = sha256_next_prime(q), 1);
    }
    self->k = k;
    for(int i = 0; i < 8; i++)
        self->h[i] = h[i];
}

static inline uint32_t sha256_ror(uint32_t n, int q)
{
    return (n >> q) | (n << (32 - q));
}

static inline uint32_t sha256_xorrotshr(uint32_t n, int a, int b, int c)
{
    return sha256_ror(n, a) ^ sha256_ror(n, b) ^ (n >> c);
}

static inline uint32_t sha256_xorrotrot(uint32_t n, int a, int b, int c)
{
    return sha256_ror(n, a) ^ sha256_ror(n, b) ^ sha256_ror(n, c);
}

void sha256_hash_block(struct sha256* self)
{
    uint32_t w[64];
    for(int i = 0; i < 16; i++)
    {
        uint32_t q = 0;
        for(int j = 4 * i; j < 4 * i + 4; j++)
            q = 256 * q + self->bytes[j];
        w[i] = q;
    }
    for(int i = 16; i < 64; i++)
        w[i] = w[i-16] + sha256_xorrotshr(w[i-15], 7, 18, 3) + w[i-7] + sha256_xorrotshr(w[i-2], 17, 19, 10);
    uint32_t hh[8];
    for(int i = 0; i < 8; i++)
        hh[i] = self->h[i];
    for(int i = 0; i < 64; i++)
    {
        uint32_t t1 = sha256_xorrotrot(hh[4], 6, 11, 25) + ((hh[4] & hh[5]) ^ (~hh[4] & hh[6])) + hh[7] + self->k[i] + w[i];
        uint32_t t2 = sha256_xorrotrot(hh[0], 2, 13, 22) + ((hh[0] & hh[1]) ^ (hh[1] & hh[2]) ^ (hh[0] & hh[2]));
        for(int i = 6; i >= 0; i--)
            hh[i+1] = hh[i];
        hh[0] = t1 + t2;
        hh[4] += t1;
    }
    for(int i = 0; i < 8; i++)
        self->h[i] += hh[i];
}

void sha256_feed(struct sha256* self, const uint8_t* data, int sz)
{
    for(int i = 0; i < sz; i++)
    {
        self->bytes[self->idx++] = data[i];
        self->total_bits += 8;
        if(self->idx == 64)
        {
            sha256_hash_block(self);
            self->idx = 0;
        }
    }
}

void sha256_close(struct sha256* self, uint8_t hash[32])
{
    self->bytes[self->idx++] = 0x80;
    if(self->idx >= 56)
    {
        while(self->idx < 64)
            self->bytes[self->idx++] = 0;
        self->idx = 0;
        sha256_hash_block(self);
    }
    while(self->idx < 56)
        self->bytes[self->idx++] = 0;
    for(int i = 56; i >= 0; i -= 8)
        self->bytes[self->idx++] = self->total_bits >> i;
    sha256_hash_block(self);
    for(int i = 0; i < 8; i++)
    {
        uint32_t word = self->h[i];
        for(int j = 4 * i + 3; j >= 4 * i; j--)
        {
            hash[j] = word;
            word >>= 8;
        }
    }
}

struct object_vtable;

enum { TYPE_INT, TYPE_STR, TYPE_DICT, TYPE_FUNC };

const char* type_names[] = {
    [TYPE_INT] = "int",
    [TYPE_STR] = "str",
    [TYPE_DICT] = "dict",
    [TYPE_FUNC] = "function"
};

struct lock_ll
{
    struct AsyncFrame* waiter;
    struct lock_ll* next;
};

struct lock
{
    int locked;
    struct lock_ll* waiters;
};

ASYNC_DECLARE(slow_lock, struct lock* lock)
ASYNC_DECLARE(slow_unlock, struct lock* lock)
#define LOCK(lock) do\
{\
    if(!(lock)->locked)\
        (lock)->locked = 1;\
    else\
        AWAIT_FN(slow_lock, (lock));\
}\
while(0)
#define UNLOCK(lock) do\
{\
    (lock)->locked = 0;\
    if((lock)->waiters)\
        AWAIT_FN(slow_unlock, (lock));\
}\
while(0)

ASYNC_DECLARE(async_spawn, struct AsyncFrame* frame; struct lock* lock)

struct object
{
    struct object_vtable* vtable;
    const str_or_int* cached_names;
    struct lock names_lock;
    struct hash_table cached_props;
    struct hash_table props_locks;
    struct cache_list_entry cle;
    object* cached_string;
    struct lock string_lock;
    object* cached_int;
    struct lock int_lock;
    int cached_type;
    struct lock type_lock;
    int cached_length;
    struct lock length_lock;
};

static inline object* link_cle(object* obj)
{
    obj->cle.ht = &obj->cached_props;
    if(cache_list)
        cache_list->prev = &obj->cle.next;
    obj->cle.next = cache_list;
    obj->cle.prev = &cache_list;
    cache_list = &obj->cle;
    if(!obj->cle.next)
        cache_list_end = &obj->cle;
    return obj;
}

ASYNC_TYPEDEF(obj_names_t, object* self; const str_or_int** ans)
ASYNC_TYPEDEF(obj_getitem_t, object* self; str_or_int key; int need; object** ans)
ASYNC_TYPEDEF(obj_call_t, object* self; object* args; object** ans; struct token* report)
ASYNC_TYPEDEF(obj_tostring_t, object* self; object** ans)
ASYNC_TYPEDEF(obj_toint_t, object* self; object** ans)
ASYNC_TYPEDEF(obj_typename_t, object* self; int* ans)

struct object_vtable
{
    obj_names_t* names;
    obj_getitem_t* getitem;
    obj_call_t* call;
    obj_tostring_t* to_string;
    obj_toint_t* to_int;
    obj_typename_t* type_name;
};

ASYNC_DECLARE(obj_getnames, object* obj; const str_or_int** ans)
ASYNC_DEFINE(obj_getnames, object* obj; const str_or_int** ans,)
{
    if(!self->obj->cached_names)
    {
        LOCK(&self->obj->names_lock);
        if(!self->obj->cached_names)
            AWAIT_PTR(obj_names_t, self->obj->vtable->names, self->obj, &self->obj->cached_names);
        UNLOCK(&self->obj->names_lock);
    }
    *self->ans = self->obj->cached_names;
} ASYNC_END()

static object hasitem_stub[0];

ASYNC_DECLARE(obj_getitem, object* obj; str_or_int key; int need; object** ans)
ASYNC_DEFINE(obj_getitem, object* obj; str_or_int key; int need; object** ans, struct lock* lock)
{
    *self->ans = hash_table_get(&self->obj->cached_props, self->key);
    if(!*self->ans || (self->need && *self->ans == hasitem_stub))
    {
        self->lock = hash_table_get(&self->obj->props_locks, self->key);
        if(!self->lock)
        {
            self->lock = NEW(struct lock, {0});
            hash_table_add_mbcle(&self->obj->props_locks, self->key, self->lock, self->obj->cle.ht == &self->obj->cached_props);
        }
        LOCK(self->lock);
        *self->ans = hash_table_get(&self->obj->cached_props, self->key);
        if(!*self->ans || (self->need && *self->ans == hasitem_stub))
        {
            AWAIT_PTR(obj_getitem_t, self->obj->vtable->getitem, self->obj, self->key, self->need, self->ans);
            hash_table_add_mbcle(&self->obj->cached_props, self->key, *self->ans, self->obj->cle.ht == &self->obj->cached_props);
        }
        UNLOCK(self->lock);
    }
    if(&self->obj->cle != cache_list && self->obj->cle.prev)
    {
        if(!self->obj->cle.next)
            cache_list_end = (struct cache_list_entry*)self->obj->cle.prev;
        *self->obj->cle.prev = self->obj->cle.next;
        if(self->obj->cle.next)
            self->obj->cle.next->prev = self->obj->cle.prev;
        self->obj->cle.next = cache_list;
        cache_list->prev = &self->obj->cle.next;
        self->obj->cle.prev = &cache_list;
        cache_list = &self->obj->cle;
    }
} ASYNC_END()

ASYNC_DECLARE(obj_call, object* obj; object* args; object** ans; struct token* report)
ASYNC_DEFINE(obj_call, object* obj; object* args; object** ans; struct token* report,)
{
    TAILCALL_PTR(obj_call_t, self->obj->vtable->call, self->obj, self->args, self->ans, self->report);
} ASYNC_END()

ASYNC_DECLARE(obj_tostring, object* obj; object** ans)
ASYNC_DEFINE(obj_tostring, object* obj; object** ans,)
{
    if(!self->obj->cached_string)
    {
        LOCK(&self->obj->string_lock);
        if(!self->obj->cached_string)
            AWAIT_PTR(obj_tostring_t, self->obj->vtable->to_string, self->obj, &self->obj->cached_string);
        UNLOCK(&self->obj->string_lock);
    }
    *self->ans = self->obj->cached_string;
} ASYNC_END()

ASYNC_DECLARE(obj_toint, object* obj; object** ans)
ASYNC_DEFINE(obj_toint, object* obj; object** ans,)
{
    if(!self->obj->cached_int)
    {
        LOCK(&self->obj->int_lock);
        if(!self->obj->cached_int)
            AWAIT_PTR(obj_toint_t, self->obj->vtable->to_int, self->obj, &self->obj->cached_int);
        UNLOCK(&self->obj->int_lock);
    }
    *self->ans = self->obj->cached_int;
} ASYNC_END()

ASYNC_DECLARE(obj_gettype, object* obj; int* ans)
ASYNC_DEFINE(obj_gettype, object* obj; int* ans, int q)
{
    if(!self->obj->cached_type)
    {
        LOCK(&self->obj->type_lock);
        if(!self->obj->cached_type)
        {
            AWAIT_PTR(obj_typename_t, self->obj->vtable->type_name, self->obj, &self->q);
            self->obj->cached_type = self->q + 1;
        }
        UNLOCK(&self->obj->type_lock);
    }
    *self->ans = self->obj->cached_type - 1;
} ASYNC_END()

ASYNC_DECLARE(obj_tostring_assert, object* obj; object** ans)
ASYNC_DEFINE(obj_tostring_assert, object* obj; object** ans, int t)
{
    AWAIT_FN(obj_tostring, self->obj, self->ans);
    if(!*self->ans)
    {
        AWAIT_FN(obj_gettype, self->obj, &self->t);
        fprintf(stderr, "error: cannot coerce %s to string\n", type_names[self->t]);
        error_exit();
    }
} ASYNC_END()

ASYNC_DECLARE(obj_toint_assert, object* obj; object** ans)
ASYNC_DEFINE(obj_toint_assert, object* obj; object** ans, int t)
{
    AWAIT_FN(obj_toint, self->obj, self->ans);
    if(!*self->ans)
    {
        AWAIT_FN(obj_gettype, self->obj, &self->t);
        fprintf(stderr, "error: cannot coerce %s to int\n", type_names[self->t]);
        error_exit();
    }
} ASYNC_END()

struct token
{
    enum { TOK_NULL, TOK_INT, TOK_STRING, TOK_IDENTIFIER, TOK_OPERATOR } which;
    union
    {
        int value_i;
        const char* value_s;
    };
    const char* file;
    int line;
    int column;
};

void error_at_token(struct token* tok)
{
    if(tok)
        fprintf(stderr, " at %s:%d:%d\n", tok->file, tok->line+1, tok->column+1);
    else
        fprintf(stderr, "\n");
    error_exit();
}

ASYNC_DECLARE(obj_call_assert, object* obj; object* args; object** ans; struct token* report)
ASYNC_DEFINE(obj_call_assert, object* obj; object* args; object** ans; struct token* report, int t)
{
    AWAIT_FN(obj_call, self->obj, self->args, self->ans, self->report);
    if(!*self->ans)
    {
        AWAIT_FN(obj_gettype, self->obj, &self->t);
        fprintf(stderr, "error: %s is not callable", type_names[self->t]);
        error_at_token(self->report);
    }
} ASYNC_END()

static inline int tokencmp(const struct token* a, const struct token* b)
{
    if(a->which != b->which)
        return 1;
    if(a->which == TOK_INT || a->which == TOK_OPERATOR)
        return a->value_i != b->value_i;
    return strcmp(a->value_s, b->value_s);
}

struct lexer_file
{
    int(*get)(void* opaque);
    void(*unget)(int c, void* opaque);
    void* opaque;
    const char* name;
    int line;
    int column;
    int prev_line;
    int prev_column;
};

static inline int lexer_file_get(struct lexer_file* f)
{
    int c = f->get(f->opaque);
    if(c == EOF)
        return EOF;
    f->prev_line = f->line;
    f->prev_column = f->column;
    if(c == '\n')
    {
        f->line++;
        f->column = 0;
    }
    else
        f->column++;
    return c;
}

static inline void lexer_file_unget(int c, struct lexer_file* f)
{
    f->unget(c, f->opaque);
    f->line = f->prev_line;
    f->column = f->prev_column;
}

struct token lex_string(struct lexer_file* f, int which)
{
    //starting " has already been read
    int line = f->prev_line;
    int column = f->prev_column;
    char smallbuf[8];
    char* ans = smallbuf;
    *ans = 0;
    int sz = 1;
    int cap = 8;
    for(;;)
    {
        int c = lexer_file_get(f);
        if(c == EOF)
        {
            fprintf(stderr, "error: EOF while parsing string literal at %s\n", f->name);
            error_exit();
        }
        if(which == TOK_IDENTIFIER)
        {
            if(c == EOF || !((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || (c == '_')))
            {
                if(c != EOF)
                    lexer_file_unget(c, f);
                ans[sz-1] = 0;
                if(!strcmp(ans, "let") || !strcmp(ans, "in"))
                {
                    int q = (unsigned char)ans[0] * 65536 + (unsigned char)ans[1] * 256 + (unsigned char)ans[2];
                    if(q % 256 == 0)
                        q >>= 8;
                    return (struct token){
                        .which = TOK_OPERATOR,
                        .value_i = q,
                        .file = f->name,
                        .line = line,
                        .column = column,
                    };
                }
                if(ans == smallbuf)
                {
                    ans = chk_malloc(sz);
                    memcpy(ans, smallbuf, sz);
                }
                return (struct token){
                    .which = TOK_IDENTIFIER,
                    .value_s = ans,
                    .file = f->name,
                    .line = line,
                    .column = column,
                };
            }
        }
        else
        {
            if(c == '"')
            {
                if(ans == smallbuf)
                {
                    ans = chk_malloc(sz);
                    memcpy(ans, smallbuf, sz);
                }
                ans[sz-1] = 0;
                return (struct token){
                    .which = TOK_STRING,
                    .value_s = ans,
                    .file = f->name,
                    .line = line,
                    .column = column,
                };
            }
            else if(c == '\\')
            {
                int c2 = lexer_file_get(f);
                if(c2 == EOF)
                {
                    fprintf(stderr, "error: EOF while parsing string literal at %s\n", f->name);
                    error_exit();
                }
                else if(c2 == 'r')
                    c = '\r';
                else if(c2 == 'n')
                    c = '\n';
                else if(c2 == 't')
                    c = '\t';
                else
                    c = c2;
            }
        }
        if(!c)
        {
            fprintf(stderr, "error: '\\0' in string literal at %s:%d:%d\n", f->name, f->prev_line+1, f->prev_column+1);
            error_exit();
        }
        do
        {
            if(sz == cap)
            {
                if(ans == smallbuf)
                {
                    ans = chk_malloc(cap*2);
                    memcpy(ans, smallbuf, cap);
                }
                else
                    ans = chk_realloc(ans, cap, cap*2);
                cap *= 2;
            }
            ans[sz-1] = (c > 255) ? 255 : c;
            sz++;
        }
        while(c >= 255 && (c -= 254));
    }
}

struct token lex_int(struct lexer_file* f, int c)
{
    int line = f->prev_line;
    int column = f->prev_column;
    int q = c - '0';
    while((c = lexer_file_get(f)) >= '0' && c <= '9')
        q = 10 * q + c - '0';
    if(c != EOF)
    {
        if(c == '_' || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
        {
            fprintf(stderr, "error: invalid character '%c' in number at %s:%d:%d\n", c, f->name, f->prev_line+1, f->prev_column+1);
            error_exit();
        }
        lexer_file_unget(c, f);
    }
    return (struct token){ .which = TOK_INT, .value_i = q, .file = f->name, .line = line, .column = column };
}

struct token lex_op(struct lexer_file* f, int c1)
{
    int line = f->line;
    int column = f->column;
    int c2 = lexer_file_get(f);
    if(c2 == EOF)
        return (struct token){ .which = TOK_OPERATOR, .value_i = c1, .file = f->name, .line = line, .column = column };
    if((c1 == '.' && c2 == '.')
    || (c1 == '=' && c2 == '=')
    || (c1 == '!' && c2 == '=')
    || (c1 == '>' && c2 == '=')
    || (c1 == '<' && c2 == '=')
    || (c1 == '&' && c2 == '&')
    || (c1 == '|' && c2 == '|')
    || (c1 == '+' && c2 == '+')
    || (c1 == '/' && c2 == '/')
    || (c1 == '*' && c2 == '*')
    || (c1 == '<' && c2 == '<')
    || (c1 == '>' && c2 == '>')
    || (c1 == '=' && c2 == '>'))
        return (struct token){ .which = TOK_OPERATOR, .value_i = 256 * c1 + c2, .file = f->name, .line = line, .column = column };
    lexer_file_unget(c2, f);
    return (struct token){ .which = TOK_OPERATOR, .value_i = c1, .file = f->name, .line = line, .column = column };
}

struct token lex(struct lexer_file* f)
{
    int c;
    for(;;)
    {
        c = lexer_file_get(f);
        if(c == EOF)
            return (struct token){ .which = TOK_NULL, .value_i = 0 };
        else if(c != ' ' && c != '\r' && c != '\n' && c != '\t')
            break;
    }
    if(c >= '0' && c <= '9')
        return lex_int(f, c);
    else if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_'))
    {
        lexer_file_unget(c, f);
        return lex_string(f, TOK_IDENTIFIER);
    }
    else if(c == '"')
        return lex_string(f, TOK_STRING);
    else
        return lex_op(f, c);
}

struct token* read_file(struct lexer_file* f)
{
    struct token* out = NEW(struct token, { .which = TOK_NULL, .value_i = 0 });
    int sz = 1;
    int cap = 1;
    struct token cur;
    while((cur = lex(f)).which != TOK_NULL)
    {
        if(sz == cap)
        {
            out = chk_realloc(out, sizeof(struct token) * cap, sizeof(struct token) * cap * 2);
            cap *= 2;
        }
        out[sz-1] = cur;
        sz++;
    }
    out[sz-1] = (struct token){ .which = TOK_NULL, .value_i = 0 };
    return out;
}

/*

letop := funcdef OR let ident=letop;... in letop
funcdef := ternary OR (...) => letop
ternary := binop OR binop ? letop : letop
binop := next(powop) OR binop OP next(powop)
powop := unop OR unop ** powop
unop := next(call) OR OP unop
call := solid OR solid ( letop... ) OR solid [ letop ] OR solid . identifier
solid := identifier OR integer OR (letop) OR [letop...] OR {ident=letop;...}

*/

ASYNC_TYPEDEF(node_eval_t, struct ast_node* self; object* scope; object** ans)

typedef struct ast_node
{
    node_eval_t* eval;
    struct token token;
} ast_node;

typedef struct
{
    ast_node vt;
    const char** keys;
    ast_node** values;
    ast_node* body;
} letop_node;

typedef struct
{
    ast_node vt;
    const char** args;
    const char** kwargs;
    ast_node** default_args;
    ast_node** default_kwargs;
    ast_node* body;
} funcdef_node;

typedef struct
{
    ast_node vt;
    ast_node* body;
    const char* glob_arg;
} glob_funcdef_node;

typedef struct
{
    ast_node vt;
    ast_node* cond;
    ast_node* left;
    ast_node* right;
} ternary_node;

typedef struct
{
    ast_node vt;
    ast_node* left;
    ast_node* right;
} binop_node;

typedef struct
{
    ast_node vt;
    ast_node* value;
} unop_node;

typedef struct
{
    ast_node vt;
    ast_node* value;
} unary_quote_dict_node;

typedef struct
{
    ast_node vt;
    ast_node* func;
    ast_node** pos_args;
    const char** kw_names;
    ast_node** kw_args;
    ast_node* glob_arg;
} call_node;

typedef struct
{
    ast_node vt;
    ast_node* value;
    ast_node* key;
} subscript_node;

typedef struct
{
    ast_node vt;
    ast_node* value;
    const char* key;
} dot_node;

typedef struct
{
    ast_node vt;
    str_or_int value;
} identifier_node;

typedef struct
{
    ast_node vt;
    const char* data;
} string_node;

typedef struct
{
    ast_node vt;
    ast_node** values;
} list_node;

typedef struct
{
    ast_node vt;
    const char** keys;
    ast_node** values;
} dict_node;

ASYNC_DECLARE(eval_node, ast_node* self; object* context; object** ans)
ASYNC_DEFINE(eval_node, ast_node* self; object* context; object** ans,)
{
    TAILCALL_PTR(node_eval_t, self->self->eval, self->self, self->context, self->ans);
} ASYNC_END()

static inline int isthisop(const struct token* tok, int op)
{
    return !tokencmp(tok, &(struct token){ .which = TOK_OPERATOR, .value_i = op });
}

int is_funcdef(struct token* stream)
{
    struct token* i = stream;
    if(!isthisop(i, '('))
        return 0;
    i++;
    if(isthisop(i, ')') || isthisop(i, '*') || isthisop(i, '*'*256+'*'))
        return 1;
    if(i->which != TOK_IDENTIFIER)
        return 0;
    i++;
    if(i->which != TOK_OPERATOR)
        return 0;
    if(i->value_i == ',' || i->value_i == '=')
        return 1;
    if(i->value_i != ')')
        return 0;
    i++;
    return isthisop(i, '='*256+'>');
}

ast_node* parse_letop(struct token**);
ast_node* parse_funcdef(struct token**);
ast_node* parse_ternary(struct token**);
ast_node* parse_binop(struct token**);
ast_node* parse_powop(struct token**);
ast_node* parse_unop(struct token**);
ast_node* parse_call(struct token**);
ast_node* parse_solid(struct token**);

ASYNC_DECLARE(eval_letop, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_funcdef, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_glob_funcdef, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_ternary, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_call, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_subscript, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_dot, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_identifier, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_string, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_list, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_dict, ast_node* self; object* scope; object** ans);

ASYNC_DECLARE(eval_add, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_sub, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_mul, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_div, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_mod, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_and, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_or, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_bitand, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_bitor, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_bitxor, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_asl, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_asr, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_eq, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_ne, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_lt, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_gt, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_le, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_ge, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_concat, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_array_concat, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_overlay, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_pow, ast_node* self; object* scope; object** ans);

ASYNC_DECLARE(eval_unary_plus, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_unary_minus, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_unary_tilde, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_unary_not, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_unary_sharp, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_unary_quote, ast_node* self; object* scope; object** ans);
ASYNC_DECLARE(eval_unary_quote_dict, ast_node* self; object* scope; object** ans);

static int strcmp_wrapper(const void* a, const void* b)
{
    return strcmp(((const str_or_int*)a)->value_s, ((const str_or_int*)b)->value_s);
}

static str_or_int* sort_and_verify_uniqueness(const char** a, const char** b, int fatal, struct token* report)
{
    int total = 0;
    for(int i = 0; a[i]; i++)
        total++;
    if(b)
        for(int i = 0; b[i]; i++)
            total++;
    str_or_int* tmp = chk_malloc(sizeof(*tmp) * (total+1));
    str_or_int* p = tmp;
    for(int i = 0; a[i]; i++)
        *p++ = MKSTR(a[i]);
    if(b)
        for(int i = 0; b[i]; i++)
            *p++ = MKSTR(b[i]);
    qsort(tmp, total, sizeof(*tmp), strcmp_wrapper);
    str_or_int* out = tmp+(total?1:0);
    for(int i = 1; i < total; i++)
        if(!strcmp(tmp[i-1].value_s, tmp[i].value_s))
        {
            if(fatal)
            {
                fprintf(stderr, "error: duplicate key %s", tmp[i].value_s);
                error_at_token(report);
            }
        }
        else
            *out++ = tmp[i];
    *out++ = MKNONE();
    return tmp;
}

void parse_kvpairs(struct token** pstream, const char*** pkeys, ast_node*** pvalues, int end, struct token* report)
{
    const char** keys = chk_calloc(sizeof(*keys));
    ast_node** values = chk_calloc(sizeof(*values));
    int len = 1;
    int cap = 1;
    while(!isthisop(*pstream, end))
    {
        if(pstream[0]->which != TOK_IDENTIFIER)
        {
            fprintf(stderr, "error: expected identifier");
            error_at_token(*pstream);
        }
        const char* name = pstream[0]->value_s;
        pstream[0]->value_s = NULL;
        pstream[0]++;
        if(!isthisop(*pstream, '='))
        {
            fprintf(stderr, "error: expected =");
            error_at_token(*pstream);
        }
        pstream[0]++;
        ast_node* value = parse_letop(pstream);
        if(!isthisop(*pstream, ';'))
        {
            fprintf(stderr, "error: expected ;");
            error_at_token(*pstream);
        }
        pstream[0]++;
        if(len == cap)
        {
            keys = chk_realloc(keys, sizeof(*keys) * cap, sizeof(*keys) * cap * 2);
            values = chk_realloc(values, sizeof(*values) * cap, sizeof(*values) * cap * 2);
            cap *= 2;
        }
        keys[len-1] = name;
        values[len-1] = value;
        len++;
    }
    pstream[0]++;
    len--;
    keys[len] = NULL;
    values[len] = NULL;
    *pkeys = keys;
    *pvalues = values;
    str_or_int* uniques = sort_and_verify_uniqueness(keys, keys+len, 1, report);
    int nuniques;
    for(nuniques = 0; uniques[nuniques].which != NONE; nuniques++);
    nuniques++;
    chk_free(uniques, sizeof(*uniques) * nuniques);
}

ast_node* parse_letop(struct token** pstream)
{
    if(!isthisop(*pstream, 'l'*65536+'e'*256+'t'))
        return parse_funcdef(pstream);
    struct token* let = pstream[0]++;
    const char** keys;
    ast_node** values;
    parse_kvpairs(pstream, &keys, &values, 'i'*256+'n', let);
    // "in" has been eaten
    if(!*keys)
    {
        fprintf(stderr, "error: empty let..in");
        error_at_token(pstream[0]-1);
    }
    struct token* p = *pstream;
    ast_node* body = parse_letop(pstream);
    return NEW(letop_node, {
        .vt = { .eval = eval_letop, .token = *p },
        .keys = keys,
        .values = values,
        .body = body
    });
}

ast_node* parse_funcdef(struct token** pstream)
{
    struct token* stream = *pstream;
    if(!is_funcdef(stream))
        return parse_ternary(pstream);
    struct token* opening = stream++; // (
    if(isthisop(stream, '*'*256+'*'))
    {
        stream++;
        if(stream->which != TOK_IDENTIFIER)
        {
            fprintf(stderr, "error: expected identifier");
            error_at_token(stream);
        }
        const char* name = stream->value_s;
        stream->value_s = NULL;
        stream++;
        if(!isthisop(stream, ')'))
        {
            fprintf(stderr, "error: expected )");
            error_at_token(stream);
        }
        stream++;
        if(!isthisop(stream, '='*256+'>'))
        {
            fprintf(stderr, "error: expected =>");
            error_at_token(stream);
        }
        stream++;
        ast_node* body = parse_letop(&stream);
        struct token* tok = *pstream;
        *pstream = stream;
        return NEW(glob_funcdef_node, {
            .vt = { .eval = eval_glob_funcdef, .token = *tok },
            .body = body,
            .glob_arg = name
        });
    }
    const char** name = chk_calloc(sizeof(*name));
    ast_node** def = chk_calloc(sizeof(*def));
    int sz = 1;
    int cap = 1;
    int npos = -1;
    if(isthisop(stream, ')'))
        sz = 0;
    else
        for(;;)
        {
            if(isthisop(stream, '*'*256+'*'))
            {
                fprintf(stderr, "error: glob argument must be the only argument declared");
                error_at_token(stream);
            }
            if(sz == cap)
            {
                name = chk_realloc(name, sizeof(*name) * cap, sizeof(*name) * cap * 2);
                def = chk_realloc(def, sizeof(*def) * cap, sizeof(*def) * cap * 2);
                cap *= 2;
            }
            if(isthisop(stream, '*'))
            {
                npos = sz;
                name[sz-1] = NULL;
                def[sz-1] = NULL;
                stream++;
            }
            else
            {
                if(stream->which != TOK_IDENTIFIER)
                {
                    fprintf(stderr, "error: expected identifier");
                    error_at_token(stream);
                }
                name[sz-1] = stream->value_s;
                stream->value_s = NULL;
                def[sz-1] = NULL;
                stream++;
                if(isthisop(stream, '='))
                {
                    stream++;
                    def[sz-1] = parse_letop(&stream);
                }
            }
            if(isthisop(stream, ')'))
                break;
            if(!isthisop(stream, ','))
            {
                fprintf(stderr, "error: expected ) or ,");
                error_at_token(stream);
            }
            stream++;
            sz++;
        }
    stream++; // )
    if(!isthisop(stream, '='*256+'>'))
    {
        fprintf(stderr, "error: expected =>");
        error_at_token(stream);
    }
    stream++;
    name[sz] = NULL;
    def[sz] = NULL;
    if(npos < 0)
        npos = sz;
    str_or_int* uniques = sort_and_verify_uniqueness(name, name+npos, 1, opening);
    int nuniques;
    for(nuniques = 0; uniques[nuniques].which != NONE; nuniques++);
    nuniques++;
    chk_free(uniques, sizeof(*uniques) * nuniques);
    funcdef_node* ans = NEW(funcdef_node, {
        .vt = { .eval = eval_funcdef, .token = **pstream },
        .args = name,
        .kwargs = name+npos,
        .default_args = def,
        .default_kwargs = def+npos,
        .body = parse_letop(&stream),
    });
    *pstream = stream;
    return &ans->vt;
}

ast_node* parse_ternary(struct token** pstream)
{
    ast_node* cond = parse_binop(pstream);
    if(!isthisop(*pstream, '?'))
        return cond;
    struct token* tok = *pstream;
    pstream[0]++;
    ast_node* body = parse_binop(pstream);
    if(!isthisop(*pstream, ':'))
    {
        fprintf(stderr, "error: expected :");
        error_at_token(*pstream);
    }
    pstream[0]++;
    ast_node* orelse = parse_binop(pstream);
    return NEW(ternary_node, {
        .vt = { .eval = eval_ternary, .token = *tok },
        .cond = cond,
        .left = body,
        .right = orelse
    });
}

static struct
{
    int op;
    node_eval_t* eval;
} binops[][7] = {
    {{'|'*256+'|', eval_or}, {0, NULL}},
    {{'&'*256+'&', eval_and}, {0, NULL}},
    {{'='*256+'=', eval_eq}, {'!'*256+'=', eval_ne}, {'>'*256+'=', eval_ge}, {'<'*256+'=', eval_le}, {'>', eval_gt}, {'<', eval_lt}, {0, NULL}},
    {{'|', eval_bitor}, {'/'*256+'/', eval_overlay}, {0, NULL}},
    {{'^', eval_bitxor}, {0, NULL}},
    {{'&', eval_bitand}, {0, NULL}},
    {{'<'*256+'<', eval_asl}, {'>'*256+'>', eval_asr}, {0, NULL}},
    {{'+', eval_add}, {'-', eval_sub}, {'+'*256+'+', eval_array_concat}, {'.'*256+'.', eval_concat}, {0, NULL}},
    {{'*', eval_mul}, {'/', eval_div}, {'%', eval_mod}, {0, NULL}},
    {{0, NULL}},
};

static ast_node* do_parse_binop(struct token** pstream, int depth)
{
    if(!binops[depth][0].op)
        return parse_powop(pstream);
    ast_node* cur = do_parse_binop(pstream, depth+1);
    while(pstream[0]->which == TOK_OPERATOR)
    {
        int i;
        struct token* tok = *pstream;
        for(i = 0; binops[depth][i].op && binops[depth][i].op != pstream[0]->value_i; i++);
        if(!binops[depth][i].op)
            return cur;
        node_eval_t* eval = binops[depth][i].eval;
        pstream[0]++;
        ast_node* next = do_parse_binop(pstream, depth+1);
        cur = NEW(binop_node, {
            .vt = { .eval = eval, .token = *tok },
            .left = cur,
            .right = next
        });
    }
    return cur;
}

ast_node* parse_binop(struct token** pstream)
{
    return do_parse_binop(pstream, 0);
}

ast_node* parse_powop(struct token** pstream)
{
    ast_node* left = parse_unop(pstream);
    if(isthisop(*pstream, '*'*256+'*'))
    {
        struct token* tok = *pstream;
        pstream[0]++;
        ast_node* right = parse_powop(pstream);
        return NEW(binop_node, {
            .vt = { .eval = eval_pow, .token = *tok },
            .left = left,
            .right = right
        });
    }
    return left;
}

ast_node* parse_unop(struct token** pstream)
{
    struct token* tok = *pstream;
    node_eval_t* eval;
    if(isthisop(*pstream, '+'))
    {
        pstream[0]++;
        eval = eval_unary_plus;
    }
    else if(isthisop(*pstream, '-'))
    {
        pstream[0]++;
        eval = eval_unary_minus;
    }
    else if(isthisop(*pstream, '~'))
    {
        pstream[0]++;
        eval = eval_unary_tilde;
    }
    else if(isthisop(*pstream, '!'))
    {
        pstream[0]++;
        eval = eval_unary_not;
    }
    else if(isthisop(*pstream, '#'))
    {
        pstream[0]++;
        eval = eval_unary_sharp;
    }
    else if(isthisop(*pstream, '\'') && !isthisop(1+*pstream, '{'))
    {
        pstream[0]++;
        eval = eval_unary_quote;
    }
    else
        return parse_call(pstream);
    return NEW(unop_node, {
        .vt = { .eval = eval, .token = *tok },
        .value = parse_unop(pstream)
    });
}

ast_node* parse_call(struct token** pstream)
{
    ast_node* cur = parse_solid(pstream);
    while(isthisop(*pstream, '(') || isthisop(*pstream, '[') || isthisop(*pstream, '.'))
    {
        if(isthisop(*pstream, '['))
        {
            struct token* tok = *pstream;
            pstream[0]++;
            ast_node* index = parse_letop(pstream);
            if(!isthisop(*pstream, ']'))
            {
                fprintf(stderr, "error: expected ]");
                error_at_token(*pstream);
            }
            pstream[0]++;
            cur = NEW(subscript_node, {
                .vt = { .eval = eval_subscript, .token = *tok },
                .value = cur,
                .key = index
            });
        }
        else if(isthisop(*pstream, '.'))
        {
            struct token* tok = *pstream;
            pstream[0]++;
            if(pstream[0]->which != TOK_IDENTIFIER)
            {
                fprintf(stderr, "error: expected identifier");
                error_at_token(*pstream);
            }
            cur = NEW(dot_node, {
                .vt = { .eval = eval_dot, .token = *tok },
                .value = cur,
                .key = pstream[0]->value_s
            });
            pstream[0]->value_s = NULL;
            pstream[0]++;
        }
        else
        {
            struct token* tok = *pstream;
            ast_node** args = chk_calloc(sizeof(*args));
            const char** argnames = NULL;
            int nargs = 1;
            int argcap = 1;
            int nnames = 0;
            int namecap = 0;
            int kwshift = -1;
            ast_node* glob_arg = NULL;
            struct token* opening = pstream[0]++; // (
            if(isthisop(*pstream, ')'))
                nargs = 0;
            else
                for(;;)
                {
                    if(isthisop(*pstream, '*'*256+'*'))
                    {
                        pstream[0]++;
                        if(glob_arg)
                        {
                            fprintf(stderr, "error: can have at most one glob argument");
                            error_at_token(*pstream);
                        }
                        glob_arg = parse_letop(pstream);
                        if(isthisop(*pstream, ')'))
                        {
                            nargs--;
                            break;
                        }
                        if(!isthisop(*pstream, ','))
                        {
                            fprintf(stderr, "error: expected ) or ,");
                            error_at_token(*pstream);
                        }
                        pstream[0]++;
                        continue;
                    }
                    if(pstream[0]->which == TOK_IDENTIFIER
                    && isthisop(1+*pstream, '='))
                    {
                        if(!argnames)
                        {
                            argnames = chk_calloc(sizeof(*argnames));
                            nnames = namecap = 1;
                            if(nargs == argcap)
                            {
                                args = chk_realloc(args, sizeof(*args) * argcap, sizeof(*args) * argcap * 2);
                                argcap *= 2;
                            }
                            args[nargs-1] = NULL;
                            kwshift = nargs;
                            nargs++;
                        }
                        if(nnames == namecap)
                        {
                            argnames = chk_realloc(argnames, sizeof(*argnames) * namecap, sizeof(*argnames) * namecap * 2);
                            namecap *= 2;
                        }
                        argnames[nnames-1] = pstream[0]->value_s;
                        nnames++;
                        pstream[0]->value_s = NULL;
                        pstream[0] += 2;
                    }
                    else if(argnames)
                    {
                        fprintf(stderr, "error: positional argument after keyword argument");
                        error_at_token(*pstream);
                    }
                    ast_node* argval = parse_letop(pstream);
                    if(nargs == argcap)
                    {
                        args = chk_realloc(args, sizeof(*args) * argcap, sizeof(*args) * argcap * 2);
                        argcap *= 2;
                    }
                    args[nargs-1] = argval;
                    if(isthisop(*pstream, ')'))
                        break;
                    if(!isthisop(*pstream, ','))
                    {
                        fprintf(stderr, "error: expected ) or ,");
                        error_at_token(*pstream);
                    }
                    nargs++;
                    pstream[0]++;
                }
            pstream[0]++;
            args[nargs] = NULL;
            if(argnames)
            {
                nnames--;
                argnames[nnames] = NULL;
            }
            else
            {
                argnames = chk_calloc(sizeof(*argnames));
                kwshift = nargs;
            }
            str_or_int* uniques = sort_and_verify_uniqueness(argnames, NULL, 1, opening);
            int nuniques;
            for(nuniques = 0; uniques[nuniques].which != NONE; nuniques++);
            nuniques++;
            chk_free(uniques, nuniques*sizeof(*uniques));
            cur = NEW(call_node, {
                .vt = { .eval = eval_call, .token = *tok },
                .func = cur,
                .pos_args = args,
                .kw_names = argnames,
                .kw_args = args + kwshift,
                .glob_arg = glob_arg
            });
        }
    }
    return cur;
}

ast_node* parse_solid(struct token** pstream)
{
    if(isthisop(*pstream, '('))
    {
        pstream[0]++; // (
        ast_node* ans = parse_letop(pstream);
        if(!isthisop(*pstream, ')'))
        {
            fprintf(stderr, "error: expected )");
            error_at_token(*pstream);
        }
        pstream[0]++;
        return ans;
    }
    if(isthisop(*pstream, '['))
    {
        struct token* tok = *pstream;
        pstream[0]++; // [
        ast_node** values = chk_calloc(sizeof(*values));
        int len = 1;
        int cap = 1;
        if(isthisop(*pstream, ']'))
            len = 0;
        else
            for(;;)
            {
                ast_node* cur = parse_letop(pstream);
                if(len == cap)
                {
                    values = chk_realloc(values, sizeof(*values) * cap, sizeof(*values) * cap * 2);
                    cap *= 2;
                }
                values[len-1] = cur;
                if(isthisop(*pstream, ']'))
                    break;
                if(!isthisop(*pstream, ','))
                {
                    fprintf(stderr, "error: expected ] or ,");
                    error_at_token(*pstream);
                }
                pstream[0]++;
                len++;
            }
        pstream[0]++;
        values[len] = NULL;
        return NEW(list_node, {
            .vt = { .eval = eval_list, .token = *tok },
            .values = values
        });
    }
    if(isthisop(*pstream, '\'') && isthisop(1+*pstream, '{'))
    {
        struct token* opening = pstream[0]++;
        return NEW(unary_quote_dict_node, {
            .vt = { .eval = eval_unary_quote_dict, .token = *opening },
            .value = parse_solid(pstream),
        });
    }
    if(isthisop(*pstream, '{'))
    {
        struct token* opening = pstream[0]++;
        const char** keys;
        ast_node** values;
        parse_kvpairs(pstream, &keys, &values, '}', opening);
        // "}" has been eaten
        return NEW(dict_node, {
            .vt = { .eval = eval_dict, .token = *opening },
            .keys = keys,
            .values = values
        });
    }
    if(pstream[0]->which == TOK_OPERATOR)
    {
        if(pstream[0]->value_i >= 65536)
            fprintf(stderr, "error: unexpected %c%c%c", pstream[0]->value_i/65536, (pstream[0]->value_i/256)%256, pstream[0]->value_i%256);
        else if(pstream[0]->value_i >= 256)
            fprintf(stderr, "error: unexpected %c%c", pstream[0]->value_i/256, pstream[0]->value_i%256);
        else
            fprintf(stderr, "error: unexpected %c", pstream[0]->value_i);
        error_at_token(*pstream);
    }
    else if(pstream[0]->which == TOK_NULL)
    {
        fprintf(stderr, "error: unexpected EOF\n");
        error_exit();
    }
    else if(pstream[0]->which == TOK_IDENTIFIER)
    {
        struct token* tok = *pstream;
        const char* name = pstream[0]->value_s;
        pstream[0]->value_s = NULL;
        pstream[0]++;
        return NEW(identifier_node, {
            .vt = { .eval = eval_identifier, .token = *tok },
            .value = MKSTR(name)
        });
    }
    else if(pstream[0]->which == TOK_STRING)
    {
        struct token* tok = *pstream;
        const char* data = pstream[0]->value_s;
        pstream[0]->value_s = NULL;
        pstream[0]++;
        return NEW(string_node, {
            .vt = { .eval = eval_string, .token = *tok },
            .data = data
        });
    }
    else if(pstream[0]->which == TOK_INT)
    {
        struct token* tok = *pstream;
        int value = pstream[0]->value_i;
        pstream[0]++;
        return NEW(identifier_node, {
            .vt = { .eval = eval_identifier, .token = *tok },
            .value = MKINT(value)
        });
    }
}

ast_node* lex_and_parse_something(struct lexer_file* f)
{
    struct token* stream0 = read_file(f);
    struct token* stream = stream0;
    ast_node* ans = parse_letop(&stream);
    if(stream->which != TOK_NULL)
    {
        fprintf(stderr, "error: unexpected data after program end\n");
        error_exit();
    }
    for(struct token* i = stream0; i->which != TOK_NULL; i++)
        if((i->which == TOK_IDENTIFIER || i->which == TOK_STRING) && i->value_s)
            chk_free((char*)i->value_s, strlen(i->value_s) + 1);
    chk_free(stream0, (stream - stream0) + 1);
    return ans;
}

static int file_get(void* opaque)
{
    return fgetc(opaque);
}

static void file_unget(int c, void* opaque)
{
    ungetc(c, opaque);
}

ast_node* lex_and_parse_file(FILE* f, const char* name)
{
    struct lexer_file ff = {
        .get = file_get,
        .unget = file_unget,
        .opaque = f,
        .name = name,
    };
    return lex_and_parse_something(&ff);
}

static int string_get(void* opaque)
{
    char** q = opaque;
    if(!**q)
        return EOF;
    return (unsigned char)*q[0]++;
}

static void string_unget(int c, void* opaque)
{
    char** q = opaque;
    q[0]--;
}

ast_node* lex_and_parse_string(const char* s, const char* name)
{
    struct lexer_file ff = {
        .get = string_get,
        .unget = string_unget,
        .opaque = &s,
        .name = name,
    };
    return lex_and_parse_something(&ff);
}

typedef struct
{
    object self;
    ast_node* node;
    object* context;
    struct lock lock;
} proxy_object;

extern struct object_vtable vt_proxy;

ASYNC_DECLARE(proxy_get_backing, object* o; object** ans)
ASYNC_DEFINE(proxy_get_backing, object* o; object** ans,)
{
    proxy_object* q = (proxy_object*)self->o;
    while(q->node)
    {
        LOCK(&q->lock);
        q = (proxy_object*)self->o;
        if(!q->node)
        {
            UNLOCK(&q->lock);
            q = (proxy_object*)self->o;
            break;
        }
        AWAIT_FN(eval_node, q->node, q->context, &q->context);
        q = (proxy_object*)self->o;
        q->node = NULL;
        UNLOCK(&q->lock);
        q = (proxy_object*)self->o;
        if(q->context->vtable != &vt_proxy)
            break;
        q = (proxy_object*)q->context;
        self->o = (object*)q;
    }
    *self->ans = q->context;
} ASYNC_END()

ASYNC_DECLARE(proxy_names, object* self; const str_or_int** ans)
ASYNC_DEFINE(proxy_names, object* self; const str_or_int** ans,)
{
    AWAIT_FN(proxy_get_backing, self->self, &self->self);
    TAILCALL_FN(obj_getnames, self->self, self->ans);
} ASYNC_END()

ASYNC_DECLARE(proxy_getitem, object* self; str_or_int key; int need; object** ans)
ASYNC_DEFINE(proxy_getitem, object* self; str_or_int key; int need; object** ans,)
{
    AWAIT_FN(proxy_get_backing, self->self, &self->self);
    TAILCALL_FN(obj_getitem, self->self, self->key, self->need, self->ans);
} ASYNC_END()

ASYNC_DECLARE(proxy_call, object* self; object* args; object** ans; struct token* report)
ASYNC_DEFINE(proxy_call, object* self; object* args; object** ans; struct token* report,)
{
    AWAIT_FN(proxy_get_backing, self->self, &self->self);
    TAILCALL_FN(obj_call, self->self, self->args, self->ans, self->report);
} ASYNC_END()

ASYNC_DECLARE(proxy_to_string, object* self; object** ans)
ASYNC_DEFINE(proxy_to_string, object* self; object** ans,)
{
    AWAIT_FN(proxy_get_backing, self->self, &self->self);
    TAILCALL_FN(obj_tostring, self->self, self->ans);
} ASYNC_END()

ASYNC_DECLARE(proxy_to_int, object* self; object** ans)
ASYNC_DEFINE(proxy_to_int, object* self; object** ans,)
{
    AWAIT_FN(proxy_get_backing, self->self, &self->self);
    TAILCALL_FN(obj_toint, self->self, self->ans);
} ASYNC_END()

ASYNC_DECLARE(proxy_get_type, object* self; int* ans)
ASYNC_DEFINE(proxy_get_type, object* self; int* ans,)
{
    AWAIT_FN(proxy_get_backing, self->self, &self->self);
    TAILCALL_FN(obj_gettype, self->self, self->ans);
} ASYNC_END()

struct object_vtable vt_proxy = {
    .names = proxy_names,
    .getitem = proxy_getitem,
    .call = proxy_call,
    .to_string = proxy_to_string,
    .to_int = proxy_to_int,
    .type_name = proxy_get_type
};

static inline object* get_proxy(ast_node* node, object* ctxt)
{
    return link_cle(NEW(proxy_object, {
        .self = { .vtable = &vt_proxy },
        .node = node,
        .context = ctxt
    }));
}

#define MERGER(name, type, cmp, none, isnone) \
type* name(type const* a, type const* b)\
{\
    int cnt = 0;\
    for(int i = 0; !(a[i] isnone); i++)\
        cnt++;\
    for(int i = 0; !(b[i] isnone); i++)\
        cnt++;\
    type* out = chk_malloc(sizeof(type)*(cnt+1));\
    type* p = out;\
    while(!((*a) isnone) || !((*b) isnone))\
    {\
        if((*a) isnone)\
            *p++ = *b++;\
        else if((*b) isnone)\
            *p++ = *a++;\
        else if(cmp) /* *a < *b */ \
            *p++ = *a++;\
        else\
        {\
            type const* tmp = a;\
            a = b;\
            b = tmp;\
            if(cmp) /* *a < *b, and we know that *a <= *b already */ \
                *p++ = *a++;\
            else\
            {\
                *p++ = *a++;\
                b++;\
            }\
        }\
    }\
    *p++ = none;\
    return out;\
}

MERGER(merge_stroi, str_or_int, (
    (a->which == INT)
    ? (b->which == STRING || b->value_i > a->value_i)
    : (b->which == STRING && strcmp(a->value_s, b->value_s) < 0)), MKNONE(), .which == NONE)

#undef MERGER

typedef struct
{
    object self;
    object* orig;
    object* scope;
} unrecur_object;

extern struct object_vtable vt_unrecur;

ASYNC_DECLARE(empty_names, object* self; const str_or_int** ans)
ASYNC_DEFINE(empty_names, object* self; const str_or_int** ans,)
{
    static str_or_int ans = MKNONE();
    *self->ans = &ans;
} ASYNC_END()

ASYNC_DECLARE(empty_getitem, object* self; str_or_int key; int need; object** ans)
ASYNC_DEFINE(empty_getitem, object* self; str_or_int key; int need; object** ans,)
{
    *self->ans = NULL;
} ASYNC_END()

static inline object* make_overlay(object* a, object* b);

ASYNC_DECLARE(dict_call, object* self; object* args; object** ans; struct token* report)
ASYNC_DEFINE(dict_call, object* self; object* args; object** ans; struct token* report,)
{
    unrecur_object* ans = (unrecur_object*)link_cle(NEW(unrecur_object, {
        .self = { .vtable = &vt_unrecur },
        .orig = self->self,
    }));
    ans->scope = make_overlay(self->args, (object*)ans);
    *self->ans = (object*)ans;
} ASYNC_END()

ASYNC_DECLARE(dict_to_string, object* self; object** ans)
ASYNC_DEFINE(dict_to_string, object* self; object** ans,)
{
    *self->ans = NULL;
} ASYNC_END()

ASYNC_DECLARE(type_name_dict, object* self; int* ans)
ASYNC_DEFINE(type_name_dict, object* self; int* ans,)
{
    *self->ans = TYPE_DICT;
} ASYNC_END()

struct object_vtable vt_empty = {
    .names = empty_names,
    .getitem = empty_getitem,
    .call = dict_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_dict
};

typedef struct
{
    object self;
    object* top;
    object* bottom;
} overlay_object;

ASYNC_DECLARE(overlay_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(overlay_names, object* o; const str_or_int** ans,
    const str_or_int* names1;
    const str_or_int* names2;
    struct lock wait1)
{
    overlay_object* q = (overlay_object*)self->o;
    AWAIT_FN(async_spawn, FRAME_FN(obj_getnames, q->top, &self->names1), &self->wait1);
    q = (overlay_object*)self->o;
    AWAIT_FN(obj_getnames, q->bottom, &self->names2);
    LOCK(&self->wait1);
    UNLOCK(&self->wait1);
    *self->ans = merge_stroi(self->names1, self->names2);
} ASYNC_END()

ASYNC_DECLARE(overlay_getitem, object* o; str_or_int key; int need; object** ans)
ASYNC_DEFINE(overlay_getitem, object* o; str_or_int key; int need; object** ans,)
{
    overlay_object* q = (overlay_object*)self->o;
    AWAIT_FN(obj_getitem, q->top, self->key, self->need, self->ans);
    q = (overlay_object*)self->o;
    if(!*self->ans)
        TAILCALL_FN(obj_getitem, q->bottom, self->key, self->need, self->ans);
} ASYNC_END()

ASYNC_DECLARE(overlay_call, object* o; object* args; object** ans; struct token* report)
ASYNC_DEFINE(overlay_call, object* o; object* args; object** ans; struct token* report, int t)
{
    overlay_object* q = (overlay_object*)self->o;
    AWAIT_FN(obj_gettype, q->bottom, &self->t);
    q = (overlay_object*)self->o;
    if(self->t == TYPE_FUNC)
        TAILCALL_FN(obj_call, q->bottom, self->args, self->ans, self->report);
    else
        TAILCALL_FN(dict_call, self->o, self->args, self->ans, self->report);
} ASYNC_END()

ASYNC_DECLARE(overlay_get_type, object* o; int* ans)
ASYNC_DEFINE(overlay_get_type, object* o; int* ans,)
{
    overlay_object* q = (overlay_object*)self->o;
    AWAIT_FN(obj_gettype, q->bottom, self->ans);
    if(*self->ans != TYPE_FUNC)
        *self->ans = TYPE_DICT;
} ASYNC_END()

struct object_vtable vt_overlay = {
    .names = overlay_names,
    .getitem = overlay_getitem,
    .call = overlay_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = overlay_get_type,
};

static inline object* make_overlay(object* a, object* b)
{
    return link_cle(NEW(overlay_object, {
        .self = { .vtable = &vt_overlay },
        .top = b,
        .bottom = a
    }));
}

static inline object* make_dict(const char** names1, ast_node** args1, const char** names2, ast_node** args2, object* scope)
{
    object* ans = NEW(object, { .vtable = &vt_empty });
    object* inner_scope = make_overlay(scope, ans);
    str_or_int* names = sort_and_verify_uniqueness(names1, names2, 1, NULL);
    str_or_int* out = names;
    for(int i = 0; names1[i]; i++)
        if(args1[i])
        {
            *out++ = MKSTR(names1[i]);
            hash_table_add(&ans->cached_props, MKSTR(names1[i]), get_proxy(args1[i], inner_scope));
        }
    if(names2)
        for(int i = 0; names2[i]; i++)
            if(args2[i])
            {
                *out++ = MKSTR(names2[i]);
                hash_table_add(&ans->cached_props, MKSTR(names2[i]), get_proxy(args2[i], inner_scope));
            }
    qsort(names, out - names, sizeof(*names), strcmp_wrapper);
    *out++ = MKNONE();
    ans->cached_names = names;
    return ans;
}

static inline object* make_list(ast_node** values, object* scope)
{
    int size;
    for(size = 0; values[size]; size++);
    NEWARR(str_or_int, keys, i, size+1, MKINT(i));
    keys[size] = MKNONE();
    object* ans = NEW(object, {
        .vtable = &vt_empty,
        .cached_names = keys,
    });
    for(int i = 0; values[i]; i++)
        hash_table_add(&ans->cached_props, MKINT(i), get_proxy(values[i], scope));
    return ans;
}

typedef struct
{
    object self;
    object* scope;
    object* defaults;
    ast_node* body;
    struct hash_table arg_mapping;
} func_object;

ASYNC_DECLARE(func_call, object* self; object* args; object** ans; struct token* report)

ASYNC_DECLARE(type_name_function, object* self; int* ans)
ASYNC_DEFINE(type_name_function, object* self; int* ans,)
{
    *self->ans = TYPE_FUNC;
} ASYNC_END()

struct object_vtable vt_func = {
    .names = empty_names,
    .getitem = empty_getitem,
    .call = func_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_function
};

typedef struct
{
    object self;
    int value;
} int_object;

typedef struct
{
    object self;
    const char* value;
    const char* shadow;
} str_object;

extern struct object_vtable vt_str;
extern struct object_vtable vt_int;

ASYNC_DECLARE(object_to_itself, object* self; object** ans)
ASYNC_DEFINE(object_to_itself, object* self; object** ans,)
{
    *self->ans = self->self;
} ASYNC_END()

static const char* object_int_itoa(object* o)
{
    int_object* self = (int_object*)o;
    int n = self->value;
    int ndigits = snprintf(NULL, 0, "%d", self->value);
    char* out = chk_malloc(ndigits+1);
    sprintf(out, "%d", self->value);
    return out;
}

ASYNC_DECLARE(object_int_to_str, object* o; object** ans)
ASYNC_DEFINE(object_int_to_str, object* o; object** ans,)
{
    *self->ans = link_cle(NEW(str_object, {
        .self = {
            .vtable = &vt_str,
        },
        .value = object_int_itoa(self->o)
    }));
} ASYNC_END()

ASYNC_DECLARE(object_str_to_int, object* o; object** ans)
ASYNC_DEFINE(object_str_to_int, object* o; object** ans,)
{
    str_object* q = (str_object*)self->o;
    const char* src = q->value;
    char* end;
    long ans = strtol(src, &end, 0);
    if(*end)
    {
        fprintf(stderr, "error: \"%s\" is not a valid integer\n", src);
        error_exit();
    }
    *self->ans = link_cle(NEW(int_object, {
        .self = {
            .vtable = &vt_int,
        },
        .value = ans
    }));
} ASYNC_END()

ASYNC_DECLARE(type_name_str, object* o; int* ans)
ASYNC_DEFINE(type_name_str, object* o; int* ans,)
{
    *self->ans = TYPE_STR;
} ASYNC_END()

ASYNC_DECLARE(type_name_int, object* o; int* ans)
ASYNC_DEFINE(type_name_int, object* o; int* ans,)
{
    *self->ans = TYPE_INT;
} ASYNC_END()

ASYNC_DECLARE(obj_length, object* obj; int* ans)
ASYNC_DEFINE(obj_length, object* obj; int* ans, object* obj2)
{
    if(!self->obj->cached_length)
    {
        LOCK(&self->obj->length_lock);
        if(!self->obj->cached_length)
        {
            AWAIT_FN(obj_gettype, self->obj, self->ans);
            if(*self->ans == TYPE_STR)
            {
                AWAIT_FN(obj_tostring, self->obj, &self->obj2);
                self->obj->cached_length = ~strlen(((str_object*)self->obj2)->value);
            }
            else
            {
                *self->ans = 0;
                for(;;)
                {
                    AWAIT_FN(obj_getitem, self->obj, MKINT(*self->ans), 0, &self->obj2);
                    if(!self->obj2)
                        break;
                    ++*self->ans;
                }
                self->obj->cached_length = ~*self->ans;
            }
        }
        UNLOCK(&self->obj->length_lock);
    }
    *self->ans = ~self->obj->cached_length;
} ASYNC_END()

ASYNC_DECLARE(str_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(str_names, object* o; const str_or_int** ans, int l)
{
    AWAIT_FN(obj_length, self->o, &self->l);
    NEWARR(str_or_int, ans, i, self->l+1, MKINT(i));
    ans[self->l] = MKNONE();
    *self->ans = ans;
} ASYNC_END()

ASYNC_DECLARE(str_getitem, object* o; str_or_int key; int need; object** ans)
ASYNC_DEFINE(str_getitem, object* o; str_or_int key; int need; object** ans,)
{
    if(self->key.which != INT || self->key.value_i < 0)
        *self->ans = NULL;
    else
    {
        str_object* q = (str_object*)self->o;
        const char* p = q->value;
        object* ans = link_cle(NEW(int_object, {
            .self = { .vtable = &vt_int },
            .value = (unsigned char)p[self->key.value_i]
        }));
        *self->ans = ans;
    }
} ASYNC_END()

struct object_vtable vt_int = {
    .names = empty_names,
    .getitem = empty_getitem,
    .call = dict_call,
    .to_string = object_int_to_str,
    .to_int = object_to_itself,
    .type_name = type_name_int
};

struct object_vtable vt_str = {
    .names = str_names,
    .getitem = str_getitem,
    .call = dict_call,
    .to_string = object_to_itself,
    .to_int = object_str_to_int,
    .type_name = type_name_str
};

typedef struct
{
    object self;
    object* a1;
    object* a2;
    int l1;
    int l2;
} array_concat_object;

ASYNC_DECLARE(array_concat_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(array_concat_names, object* o; const str_or_int** ans,)
{
    array_concat_object* q = (array_concat_object*)self->o;
    NEWARR(str_or_int, ans, i, q->l1+q->l2+1, MKINT(i));
    ans[q->l1+q->l2] = MKNONE();
    *self->ans = ans;
} ASYNC_END()

ASYNC_DECLARE(array_concat_getitem, object* o; str_or_int idx; int need; object** ans)
ASYNC_DEFINE(array_concat_getitem, object* o; str_or_int idx; int need; object** ans,)
{
    array_concat_object* q = (array_concat_object*)self->o;
    if(self->idx.which != INT)
        *self->ans = NULL;
    else if(self->idx.value_i < 0)
        *self->ans = NULL;
    else if(!self->need && self->idx.value_i < q->l1 + q->l2)
        *self->ans = hasitem_stub;
    else if(self->idx.value_i < q->l1)
        TAILCALL_FN(obj_getitem, q->a1, self->idx, self->need, self->ans);
    else if(self->idx.value_i - q->l1 < q->l2)
        TAILCALL_FN(obj_getitem, q->a2, MKINT(self->idx.value_i - q->l1), self->need, self->ans);
    else
        *self->ans = NULL;
} ASYNC_END()

struct object_vtable vt_array_concat = {
    .names = array_concat_names,
    .getitem = array_concat_getitem,
    .call = dict_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_dict
};

typedef struct 
{
    object self;
    object* args;
    object* defaults;
    struct hash_table* mapping;
} args_wrapper_object;

ASYNC_DECLARE(args_wrapper_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(args_wrapper_names, object* o; const str_or_int** ans,)
{
    args_wrapper_object* q = (args_wrapper_object*)self->o;
    str_or_int* ans = chk_malloc(sizeof(*ans) * (q->mapping->size + 1));
    str_or_int* p = ans;
    for(int i = 0; i < q->mapping->capacity; i++)
        for(struct hash_bucket* hb = q->mapping->buckets[i]; hb; hb = hb->next)
            *p++ = hb->si;
    *p++ = MKNONE();
    *self->ans = ans;
} ASYNC_END()

ASYNC_DECLARE(args_wrapper_getitem, object* o; str_or_int key; int need; object** ans)
ASYNC_DEFINE(args_wrapper_getitem, object* o; str_or_int key; int need; object** ans,)
{
    args_wrapper_object* q = (args_wrapper_object*)self->o;
    int idx = (intptr_t)hash_table_get(q->mapping, self->key);
    if(idx < 0)
        AWAIT_FN(obj_getitem, q->args, self->key, self->need, self->ans);
    else
        AWAIT_FN(obj_getitem, q->args, MKINT(idx - 1), self->need, self->ans);
    q = (args_wrapper_object*)self->o;
    if(!*self->ans)
        TAILCALL_FN(obj_getitem, q->defaults, self->key, self->need, self->ans);
} ASYNC_END()

struct object_vtable vt_args_wrapper = {
    .names = args_wrapper_names,
    .getitem = args_wrapper_getitem,
    .call = dict_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_dict
};

ASYNC_DECLARE(unrecur_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(unrecur_names, object* o; const str_or_int** ans,)
{
    unrecur_object* q = (unrecur_object*)self->o;
    TAILCALL_FN(obj_getnames, q->orig, self->ans);
} ASYNC_END()

ASYNC_DECLARE(unrecur_getitem, object* o; str_or_int key; int need; object** ans)
ASYNC_DEFINE(unrecur_getitem, object* o; str_or_int key; int need; object** ans,)
{
    unrecur_object* q = (unrecur_object*)self->o;
    AWAIT_FN(obj_getitem, q->orig, self->key, self->need, self->ans);
    q = (unrecur_object*)self->o;
    if(*self->ans)
    {
        if(self->need)
            TAILCALL_FN(obj_call_assert, *self->ans, q->scope, self->ans, NULL);
        else
            *self->ans = hasitem_stub;
    }
} ASYNC_END()

struct object_vtable vt_unrecur = {
    .names = unrecur_names,
    .getitem = unrecur_getitem,
    .call = dict_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_dict
};

typedef struct
{
    object self;
    object* backing;
} recur_object;

ASYNC_DECLARE(returner_call, object* o; object* args; object** ans; struct token* report)
ASYNC_DEFINE(returner_call, object* o; object* args; object** ans; struct token* report,)
{
    recur_object* q = (recur_object*)self->o;
    *self->ans = q->backing;
} ASYNC_END()

struct object_vtable vt_returner = {
    .names = empty_names,
    .getitem = empty_getitem,
    .call = returner_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_function
};

ASYNC_DECLARE(recur_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(recur_names, object* o; const str_or_int** ans,)
{
    recur_object* q = (recur_object*)self->o;
    TAILCALL_FN(obj_getnames, q->backing, self->ans);
} ASYNC_END()

ASYNC_DECLARE(recur_getitem, object* o; str_or_int key; int need; object** ans)
ASYNC_DEFINE(recur_getitem, object* o; str_or_int key; int need; object** ans,)
{
    recur_object* q = (recur_object*)self->o;
    AWAIT_FN(obj_getitem, q->backing, self->key, self->need, self->ans);
    if(*self->ans)
    {
        if(self->need)
            *self->ans = link_cle(NEW(recur_object, {
                .self = { .vtable = &vt_returner },
                .backing = *self->ans
            }));
        else
            *self->ans = hasitem_stub;
    }
} ASYNC_END()

struct object_vtable vt_recur = {
    .names = recur_names,
    .getitem = recur_getitem,
    .call = dict_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_dict
};

typedef struct
{
    object self;
    object* scope;
    ast_node* body;
    const char* arg_name;
} glob_func_object;

ASYNC_DECLARE(glob_func_call, object* o; object* args; object** ans; struct token* report)
ASYNC_DEFINE(glob_func_call, object* o; object* args; object** ans; struct token* report,)
{
    glob_func_object* q = (glob_func_object*)self->o;
    str_or_int* names = chk_malloc(sizeof(*names)*2);
    names[0] = MKSTR(q->arg_name);
    names[1] = MKNONE();
    object* args_dict = NEW(object, {
        .vtable = &vt_empty,
        .cached_names = names
    });
    hash_table_add(&args_dict->cached_props, MKSTR(q->arg_name), self->args);
    args_dict = make_overlay(q->scope, args_dict);
    *self->ans = get_proxy(q->body, args_dict);
} ASYNC_END()

struct object_vtable vt_glob_func = {
    .names = empty_names,
    .getitem = empty_getitem,
    .call = glob_func_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_function
};

ASYNC_DECLARE(shift_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(shift_names, object* o; const str_or_int** ans,)
{
    AWAIT_FN(obj_getnames, ((recur_object*)self->o)->backing, self->ans);
    const str_or_int* names0 = *self->ans;
    int l;
    for(l = 0; names0[l].which != NONE; l++);
    str_or_int* ans = chk_malloc(sizeof(*ans)*(l+1));
    str_or_int* p = ans;
    for(int i = 0; names0[i].which != NONE; i++)
    {
        if(names0[i].which == STRING || names0[i].value_i < 0)
            *p++ = names0[i];
        else if(names0[i].value_i > 0)
            *p++ = MKINT(names0[i].value_i - 1);
    }
    *p++ = MKNONE();
    *self->ans = ans;
} ASYNC_END()

ASYNC_DECLARE(shift_getitem, object* o; str_or_int key; int need; object** ans)
ASYNC_DEFINE(shift_getitem, object* o; str_or_int key; int need; object** ans,)
{
    if(self->key.which == INT && self->key.value_i >= 0)
        self->key.value_i++;
    TAILCALL_FN(obj_getitem, ((recur_object*)self->o)->backing, self->key, self->need, self->ans);
} ASYNC_END()

struct object_vtable vt_shift = {
    .names = shift_names,
    .getitem = shift_getitem,
    .call = dict_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_dict
};

ASYNC_TYPEDEF(syscall_t, object* args; void* opaque; object** ans)

typedef struct
{
    object self;
    syscall_t* call;
    void* opaque;
} syscall_object;

ASYNC_DECLARE(syscall_call, object* o; object* args; object** ans; struct token* report)
ASYNC_DEFINE(syscall_call, object* o; object* args; object** ans; struct token* report,)
{
    syscall_object* q = (syscall_object*)self->o;
    TAILCALL_PTR(syscall_t, q->call, self->args, q->opaque, self->ans);
} ASYNC_END()

typedef struct
{
    object self;
    char c;
} char_object;

ASYNC_DECLARE(char_names, object* o; const str_or_int** ans)
ASYNC_DEFINE(char_names, object* o; const str_or_int** ans,)
{
    str_or_int* names = chk_malloc(sizeof(*names)*2);
    names[0].which = INT;
    names[0].value_i = 0;
    names[1].which = NONE;
    *self->ans = names;
} ASYNC_END()

ASYNC_DECLARE(char_getitem, object* o; str_or_int key; int need; object** ans)
ASYNC_DEFINE(char_getitem, object* o; str_or_int key; int need; object** ans,)
{
    static char dummy;
    if(self->key.which != INT || self->key.value_i != 0)
    {
        *self->ans = NULL;
        RETURN();
    }
    if(!self->need)
    {
        *self->ans = (void*)&dummy;
        RETURN();
    }
    char_object* q = (char_object*)self->o;
    *self->ans = NEW(int_object, {
        .self = { .vtable = &vt_int, },
        .value = (unsigned char)q->c,
    });
} ASYNC_END()

ASYNC_DECLARE(char_to_string, object* o; object** ans)
ASYNC_DEFINE(char_to_string, object* o; object** ans,)
{
    char_object* q = (char_object*)self->o;
    char* mem = chk_malloc(2);
    mem[0] = q->c;
    mem[1] = 0;
    *self->ans = NEW(str_object, {
        .self = { .vtable = &vt_str, },
        .value = mem,
    });
} ASYNC_END()

ASYNC_DECLARE(char_to_int, object* o; object** ans)
ASYNC_DEFINE(char_to_int, object* o; object** ans,)
{
    char_object* q = (char_object*)self->o;
    if(q->c >= '0' && q->c <= '9')
        *self->ans = NEW(int_object, {
            .self = { .vtable = &vt_int, },
            .value = q->c - '0',
        });
    else
        *self->ans = NULL;
} ASYNC_END()

struct object_vtable vt_char = {
    .names = char_names,
    .getitem = char_getitem,
    .call = dict_call,
    .to_string = char_to_string,
    .to_int = char_to_int,
    .type_name = type_name_str,
};

struct object_vtable vt_syscall = {
    .names = empty_names,
    .getitem = empty_getitem,
    .call = syscall_call,
    .to_string = dict_to_string,
    .to_int = dict_to_string,
    .type_name = type_name_function
};

ASYNC_DEFINE(eval_letop, ast_node* o; object* scope; object** ans,)
{
    letop_node* q = (letop_node*)self->o;
    *self->ans = get_proxy(q->body, make_overlay(self->scope, make_dict(q->keys, q->values, NULL, NULL, self->scope)));
} ASYNC_END()

ASYNC_DEFINE(eval_funcdef, ast_node* o; object* scope; object** ans,)
{
    funcdef_node* q = (funcdef_node*)self->o;
    func_object* ans = (func_object*)link_cle(NEW(func_object, {
        .self = { .vtable = &vt_func },
        .scope = self->scope,
        .defaults = make_dict(q->args, q->default_args, q->kwargs, q->default_kwargs, self->scope),
        .body = q->body
    }));
    for(int i = 0; q->args[i]; i++)
        hash_table_add(&ans->arg_mapping, MKSTR(q->args[i]), (void*)(intptr_t)(i+1));
    for(int i = 0; q->kwargs[i]; i++)
        hash_table_add(&ans->arg_mapping, MKSTR(q->kwargs[i]), (void*)(intptr_t)-1);
    *self->ans = &ans->self;
} ASYNC_END()

ASYNC_DEFINE(eval_glob_funcdef, ast_node* o; object* scope; object** ans,)
{
    glob_funcdef_node* q = (glob_funcdef_node*)self->o;
    *self->ans = link_cle(NEW(glob_func_object, {
        .self = { .vtable = &vt_glob_func },
        .body = q->body,
        .scope = self->scope,
        .arg_name = q->glob_arg
    }));
} ASYNC_END()

ASYNC_DEFINE(eval_ternary, ast_node* o; object* scope; object** ans,)
{
    ternary_node* q = (ternary_node*)self->o;
    AWAIT_FN(eval_node, q->cond, self->scope, self->ans);
    AWAIT_FN(obj_toint_assert, *self->ans, self->ans);
    q = (ternary_node*)self->o;
    int_object* obj = (int_object*)*self->ans;
    *self->ans = get_proxy(obj->value ? q->left : q->right, self->scope);
} ASYNC_END()

ASYNC_DEFINE(eval_call, ast_node* o; object* scope; object** ans, object* fn)
{
    call_node* q = (call_node*)self->o;
    AWAIT_FN(eval_node, q->func, self->scope, &self->fn);
    q = (call_node*)self->o;
    object* args = make_list(q->pos_args, self->scope);
    object* kwargs = make_dict(q->kw_names, q->kw_args, NULL, NULL, self->scope);
    args = make_overlay(args, kwargs);
    if(q->glob_arg)
        args = make_overlay(get_proxy(q->glob_arg, self->scope), args);
    TAILCALL_FN(obj_call_assert, self->fn, args, self->ans, &self->o->token);
} ASYNC_END()

ASYNC_DEFINE(eval_subscript, ast_node* o; object* scope; object** ans,
    object* obj;
    object* key;
    int tp;
    struct lock wait_obj;
    str_or_int kk)
{
    subscript_node* q = (subscript_node*)self->o;
    AWAIT_FN(async_spawn, FRAME_FN(eval_node, q->value, self->scope, &self->obj), &self->wait_obj);
    q = (subscript_node*)self->o;
    AWAIT_FN(eval_node, q->key, self->scope, &self->key);
    AWAIT_FN(obj_gettype, self->key, &self->tp);
    if(self->tp == TYPE_INT)
    {
        AWAIT_FN(obj_toint_assert, self->key, self->ans);
        self->kk = MKINT(((int_object*)*self->ans)->value);
    }
    else if(self->tp == TYPE_STR)
    {
        AWAIT_FN(obj_tostring_assert, self->key, self->ans);
        self->kk = MKSTR(((str_object*)*self->ans)->value);
    }
    else
    {
        fprintf(stderr, "error: subscript index must be int or str");
        error_at_token(&self->o->token);
    }
    LOCK(&self->wait_obj);
    UNLOCK(&self->wait_obj);
    AWAIT_FN(obj_getitem, self->obj, self->kk, 1, self->ans);
    if(!*self->ans)
    {
        if(self->kk.which == INT)
            fprintf(stderr, "error: dict does not have key %d", self->kk.value_i);
        else
            fprintf(stderr, "error: dict does not have key \"%s\"", self->kk.value_s);
        error_at_token(&self->o->token);
    }
} ASYNC_END()

ASYNC_DEFINE(eval_dot, ast_node* o; object* scope; object** ans,)
{
    dot_node* q = (dot_node*)self->o;
    AWAIT_FN(eval_node, q->value, self->scope, self->ans);
    q = (dot_node*)self->o;
    AWAIT_FN(obj_getitem, *self->ans, MKSTR(q->key), 1, self->ans);
    q = (dot_node*)self->o;
    if(!*self->ans)
    {
        fprintf(stderr, "error: dict does not have key \"%s\"", q->key);
        error_at_token(&self->o->token);
    }
} ASYNC_END()

ASYNC_DEFINE(eval_identifier, ast_node* o; object* scope; object** ans,)
{
    identifier_node* q = (identifier_node*)self->o;
    if(q->value.which == INT)
        *self->ans = link_cle(NEW(int_object, {
            .self = { .vtable = &vt_int },
            .value = q->value.value_i
        }));
    else
    {
        AWAIT_FN(obj_getitem, self->scope, q->value, 1, self->ans);
        q = (identifier_node*)self->o;
        if(!*self->ans)
        {
            fprintf(stderr, "error: undefined reference to %s", q->value.value_s);
            error_at_token(&self->o->token);
        }
    }
} ASYNC_END()

ASYNC_DEFINE(eval_string, ast_node* o; object* scope; object** ans,)
{
    string_node* q = (string_node*)self->o;
    *self->ans = link_cle(NEW(str_object, {
        .self = { .vtable = &vt_str },
        .value = q->data
    }));
} ASYNC_END()

ASYNC_DEFINE(eval_list, ast_node* o; object* scope; object** ans,)
{
    list_node* q = (list_node*)self->o;
    *self->ans = make_list(q->values, self->scope);
} ASYNC_END()

ASYNC_DEFINE(eval_dict, ast_node* o; object* scope; object** ans,)
{
    dict_node* q = (dict_node*)self->o;
    *self->ans = make_dict(q->keys, q->values, NULL, NULL, self->scope);
} ASYNC_END()

ASYNC_DECLARE(eval_toint_assert, ast_node* o; object* scope; object** ans)
ASYNC_DEFINE(eval_toint_assert, ast_node* o; object* scope; object** ans,)
{
    AWAIT_FN(eval_node, self->o, self->scope, self->ans);
    TAILCALL_FN(obj_toint_assert, *self->ans, self->ans);
} ASYNC_END()

#define ARITHMETIC(name, expr)\
ASYNC_DEFINE(name, ast_node* o; object* scope; object** ans,\
    object* oa;\
    object* ob;\
    struct lock oa_wait)\
{\
    binop_node* q = (binop_node*)self->o;\
    AWAIT_FN(async_spawn, FRAME_FN(eval_toint_assert, q->left, self->scope, &self->oa), &self->oa_wait);\
    q = (binop_node*)self->o;\
    AWAIT_FN(eval_toint_assert, q->right, self->scope, &self->ob);\
    LOCK(&self->oa_wait);\
    UNLOCK(&self->oa_wait);\
    int a = ((int_object*)self->oa)->value;\
    int b = ((int_object*)self->ob)->value;\
    *self->ans = link_cle(NEW(int_object, {\
        .self = {\
            .vtable = &vt_int,\
        },\
        .value = expr\
    }));\
} ASYNC_END()

ARITHMETIC(eval_add, a + b)
ARITHMETIC(eval_sub, a - b)
ARITHMETIC(eval_mul, a * b)
ARITHMETIC(eval_div, (b == 0 ? (fprintf(stderr, "error: division by zero"), error_at_token(&self->o->token), 0) : a / b))
ARITHMETIC(eval_mod, (b == 0 ? (fprintf(stderr, "error: modulo by zero"), error_at_token(&self->o->token), 0) : a % b))

ASYNC_DEFINE(eval_and, ast_node* o; object* scope; object** ans,)
{
    binop_node* q = (binop_node*)self->o;
    AWAIT_FN(eval_toint_assert, q->left, self->scope, self->ans);
    q = (binop_node*)self->o;
    int cond = ((int_object*)*self->ans)->value;
    if(cond)
        AWAIT_FN(eval_toint_assert, q->right, self->scope, self->ans);
} ASYNC_END()

ASYNC_DEFINE(eval_or, ast_node* o; object* scope; object** ans,)
{
    binop_node* q = (binop_node*)self->o;
    AWAIT_FN(eval_toint_assert, q->left, self->scope, self->ans);
    q = (binop_node*)self->o;
    int cond = ((int_object*)*self->ans)->value;
    if(!cond)
        AWAIT_FN(eval_toint_assert, q->right, self->scope, self->ans);
} ASYNC_END()

ARITHMETIC(eval_bitand, a & b)
ARITHMETIC(eval_bitor, a | b)
ARITHMETIC(eval_bitxor, a ^ b)
ARITHMETIC(eval_asl, (b >= 0 && b < 32) ? (a << b) : 0)
ARITHMETIC(eval_asr, (b >= 0 && b < 32) ? (a >> b) : 0)

ASYNC_DECLARE(eval_gettype, ast_node* o; object* scope; object** obj; int* ans)
ASYNC_DEFINE(eval_gettype, ast_node* o; object* scope; object** obj; int* ans,)
{
    AWAIT_FN(eval_node, self->o, self->scope, self->obj);
    AWAIT_FN(obj_gettype, *self->obj, self->ans);
} ASYNC_END()

#define COMPARISON(name, op)\
ASYNC_DEFINE(name, ast_node* o; object* scope; object** ans,\
    object* a;\
    int t1;\
    object* b;\
    int t2;\
    struct lock a_wait)\
{\
    binop_node* q = (binop_node*)self->o;\
    AWAIT_FN(async_spawn, FRAME_FN(eval_gettype, q->left, self->scope, &self->a, &self->t1), &self->a_wait);\
    q = (binop_node*)self->o;\
    AWAIT_FN(eval_gettype, q->right, self->scope, &self->b, &self->t2);\
    LOCK(&self->a_wait);\
    UNLOCK(&self->a_wait);\
    int cond;\
    if(self->t1 == TYPE_INT && self->t2 == TYPE_INT)\
    {\
        AWAIT_FN(async_spawn, FRAME_FN(obj_toint_assert, self->a, &self->a), &self->a_wait);\
        AWAIT_FN(obj_toint_assert, self->b, &self->b);\
        LOCK(&self->a_wait);\
        UNLOCK(&self->a_wait);\
        int i1 = ((int_object*)self->a)->value;\
        int i2 = ((int_object*)self->b)->value;\
        cond = i1 op i2;\
    }\
    else\
    {\
        AWAIT_FN(async_spawn, FRAME_FN(obj_tostring_assert, self->a, &self->a), &self->a_wait);\
        AWAIT_FN(obj_tostring_assert, self->b, &self->b);\
        LOCK(&self->a_wait);\
        UNLOCK(&self->a_wait);\
        const char* s1 = ((str_object*)self->a)->value;\
        const char* s2 = ((str_object*)self->b)->value;\
        cond = strcmp(s1, s2) op 0;\
    }\
    *self->ans = link_cle(NEW(int_object, {\
        .self = { .vtable = &vt_int },\
        .value = cond ? 1 : 0\
    }));\
} ASYNC_END()

COMPARISON(eval_eq, ==)
COMPARISON(eval_ne, !=)
COMPARISON(eval_lt, <)
COMPARISON(eval_gt, >)
COMPARISON(eval_le, <=)
COMPARISON(eval_ge, >=)

#undef COMPARISON

ASYNC_DECLARE(eval_tostring_assert, ast_node* o; object* scope; const char** ans)
ASYNC_DEFINE(eval_tostring_assert, ast_node* o; object* scope; const char** ans,
    object* obj)
{
    AWAIT_FN(eval_node, self->o, self->scope, &self->obj);
    AWAIT_FN(obj_tostring_assert, self->obj, &self->obj);
    *self->ans = ((str_object*)self->obj)->value;
} ASYNC_END()

ASYNC_DEFINE(eval_concat, ast_node* o; object* scope; object** ans,
    const char* s1;
    const char* s2;
    struct lock s1_wait)
{
    binop_node* q = (binop_node*)self->o;
    AWAIT_FN(async_spawn, FRAME_FN(eval_tostring_assert, q->left, self->scope, &self->s1), &self->s1_wait);
    q = (binop_node*)self->o;
    AWAIT_FN(eval_tostring_assert, q->right, self->scope, &self->s2);
    LOCK(&self->s1_wait);
    UNLOCK(&self->s1_wait);
    int l1 = strlen(self->s1);
    int l2 = strlen(self->s2);
    char* ans = chk_malloc(l1+l2+1);
    memcpy(ans, self->s1, l1);
    memcpy(ans+l1, self->s2, l2+1);
    *self->ans = link_cle(NEW(str_object, {
        .self = {
            .vtable = &vt_str,
        },
        .value = ans
    }));
} ASYNC_END()

ASYNC_DECLARE(eval_array_length, ast_node* o; object* scope; object** ans; int* len)
ASYNC_DEFINE(eval_array_length, ast_node* o; object* scope; object** ans; int* len,)
{
    AWAIT_FN(eval_node, self->o, self->scope, self->ans);
    AWAIT_FN(obj_length, *self->ans, self->len);
} ASYNC_END()

ASYNC_DEFINE(eval_array_concat, ast_node* o; object* scope; object** ans,
    object* a;
    int l1;
    object* b;
    int l2;
    struct lock a_wait)
{
    binop_node* q = (binop_node*)self->o;
    AWAIT_FN(async_spawn, FRAME_FN(eval_array_length, q->left, self->scope, &self->a, &self->l1), &self->a_wait);
    q = (binop_node*)self->o;
    AWAIT_FN(eval_array_length, q->right, self->scope, &self->b, &self->l2);
    LOCK(&self->a_wait);
    UNLOCK(&self->a_wait);
    *self->ans = link_cle(NEW(array_concat_object, {
        .self = { .vtable = &vt_array_concat },
        .a1 = self->a,
        .a2 = self->b,
        .l1 = self->l1,
        .l2 = self->l2
    }));
} ASYNC_END()

ASYNC_DEFINE(eval_overlay, ast_node* o; object* scope; object** ans,)
{
    binop_node* q = (binop_node*)self->o;
    *self->ans = make_overlay(get_proxy(q->left, self->scope), get_proxy(q->right, self->scope));
} ASYNC_END()

static inline int int_pow(int a, int b)
{
    if(b == 0)
        return 1;
    if(b % 2)
        return a * int_pow(a, b - 1);
    return int_pow(a * a, b / 2);
}

ARITHMETIC(eval_pow, int_pow(a, b))

#undef ARITHMETIC

#define UNARY_ARITHMETIC(name, op)\
ASYNC_DEFINE(name, ast_node* o; object* scope; object** ans,)\
{\
    unop_node* q = (unop_node*)self->o;\
    AWAIT_FN(eval_toint_assert, q->value, self->scope, self->ans);\
    int val = ((int_object*)*self->ans)->value;\
    *self->ans = link_cle(NEW(int_object, {\
        .self = {\
            .vtable = &vt_int,\
        },\
        .value = op\
    }));\
} ASYNC_END()

UNARY_ARITHMETIC(eval_unary_plus, +val)
UNARY_ARITHMETIC(eval_unary_minus, -val)
UNARY_ARITHMETIC(eval_unary_tilde, ~val)
UNARY_ARITHMETIC(eval_unary_not, val ? 0 : 1)

#undef UNARY_ARITHMETIC

ASYNC_DEFINE(eval_unary_sharp, ast_node* o; object* scope; object** ans, int len)
{
    unop_node* q = (unop_node*)self->o;
    AWAIT_FN(eval_node, q->value, self->scope, self->ans);
    AWAIT_FN(obj_length, *self->ans, &self->len);
    *self->ans = link_cle(NEW(int_object, {
        .self = {
            .vtable = &vt_int,
        },
        .value = self->len
    }));
} ASYNC_END()

ASYNC_DEFINE(eval_unary_quote, ast_node* o; object* scope; object** ans,)
{
    unop_node* q = (unop_node*)self->o;
    AWAIT_FN(eval_node, q->value, self->scope, self->ans);
    *self->ans = link_cle(NEW(recur_object, {
        .self = { .vtable = &vt_recur },
        .backing = *self->ans
    }));
} ASYNC_END()

ASYNC_DEFINE(eval_unary_quote_dict, ast_node* o; object* scope; object** ans,)
{
    unary_quote_dict_node* q = (unary_quote_dict_node*)self->o;
    dict_node* d = (dict_node*)q->value;
    object* ans = NEW(object, { .vtable = &vt_empty });
    str_or_int* names = sort_and_verify_uniqueness(d->keys, NULL, 1, &q->vt.token);
    for(int i = 0; d->keys[i]; i++)
        hash_table_add(&ans->cached_props, MKSTR(d->keys[i]), link_cle(NEW(func_object, {
            .self = { .vtable = &vt_func },
            .scope = self->scope,
            .defaults = NULL,
            .body = d->values[i]
        })));
    ans->cached_names = names;
    *self->ans = ans;
} ASYNC_END()

ASYNC_DECLARE(func_check_getitem, object* obj; str_or_int key; struct token* report)
ASYNC_DEFINE(func_check_getitem, object* obj; str_or_int key; struct token* report,)
{
    AWAIT_FN(obj_getitem, self->obj, self->key, 0, &self->obj);
    if(!self->obj)
    {
        fprintf(stderr, "error: required argument \"%s\" not passed", self->key.value_s);
        error_at_token(self->report);
    }
} ASYNC_END()

ASYNC_DECLARE(func_check_arguments, object* args; struct hash_table* map; struct token* report)
ASYNC_DEFINE(func_check_arguments, object* args; struct hash_table* map; struct token* report,
    struct lock* waits;
    int i;
    int j;
    struct hash_bucket* hb)
{
    self->waits = chk_calloc(sizeof(*self->waits) * self->map->size);
    self->j = 0;
    for(self->i = 0; self->i < self->map->capacity; self->i++)
        for(self->hb = self->map->buckets[self->i]; self->hb; self->hb = self->hb->next)
            AWAIT_FN(async_spawn, FRAME_FN(func_check_getitem, self->args, self->hb->si, self->report), self->waits+(self->j++));
    for(self->i = 0; self->i < self->j; self->i++)
    {
        LOCK(self->waits+self->i);
        UNLOCK(self->waits+self->i);
    }
    chk_free(self->waits, sizeof(*self->waits) * self->j);
} ASYNC_END()

ASYNC_DEFINE(func_call, object* o; object* args; object** ans; struct token* report,
    const str_or_int* names)
{
    func_object* q = (func_object*)self->o;
    if(q->defaults)
    {
        AWAIT_FN(obj_getnames, self->args, &self->names);
        q = (func_object*)self->o;
        int npos = 0;
        for(int i = 0; i < q->arg_mapping.capacity; i++)
            for(struct hash_bucket* hb = q->arg_mapping.buckets[i]; hb; hb = hb->next)
                if((intptr_t)hb->value > 0)
                    npos++;
        for(int i = 0; self->names[i].which != NONE; i++)
            if(self->names[i].which == INT && self->names[i].value_i >= npos)
            {
                fprintf(stderr, "error: excess positional argument %d passed", self->names[i].value_i+1);
                error_at_token(self->report);
            }
            else if(self->names[i].which == STRING && (intptr_t)hash_table_get(&q->arg_mapping, self->names[i]) >= 0)
            {
                fprintf(stderr, "error: unsupported keyword argument \"%s\" passed", self->names[i].value_s);
                error_at_token(self->report);
            }
        self->args = link_cle(NEW(args_wrapper_object, {
            .self = { .vtable = &vt_args_wrapper },
            .args = self->args,
            .defaults = q->defaults,
            .mapping = &q->arg_mapping
        }));
        AWAIT_FN(func_check_arguments, self->args, &q->arg_mapping, self->report);
        q = (func_object*)self->o;
    }
    *self->ans = get_proxy(q->body, make_overlay(q->scope, self->args));
} ASYNC_END()

ASYNC_DECLARE(debug_print, object* obj)
ASYNC_DEFINE(debug_print, object* obj,
    int type;
    const str_or_int* names;
    object* obj1;
    int i)
{
    AWAIT_FN(obj_gettype, self->obj, &self->type);
    if(self->type == TYPE_INT)
    {
        AWAIT_FN(obj_toint_assert, self->obj, &self->obj);
        printf("%d", ((int_object*)self->obj)->value);
    }
    else if(self->type == TYPE_STR)
    {
        AWAIT_FN(obj_tostring_assert, self->obj, &self->obj);
        const char* s = ((str_object*)self->obj)->value;
        putchar('"');
        while(*s)
        {
            int c = *s++;
            if(c == '\n')
            {
                putchar('\\');
                putchar('n');
            }
            else if(c == '\r')
            {
                putchar('\\');
                putchar('r');
            }
            else if(c == '\t')
            {
                putchar('\\');
                putchar('t');
            }
            else if(c == '\\' || c == '"')
            {
                putchar('\\');
                putchar(c);
            }
            else
                putchar(c);
        }
        putchar('"');
    }
    else if(self->type == TYPE_FUNC)
        printf("<function>");
    if(self->type != TYPE_STR)
    {
        AWAIT_FN(obj_getnames, self->obj, &self->names);
        if(self->type == TYPE_DICT || self->names[0].which != NONE)
        {
            if(self->type != TYPE_DICT)
                printf(" // ");
            printf("{ ");
            for(self->i = 0; self->names[self->i].which != NONE; self->i++)
            {
                if(self->names[self->i].which == INT)
                    printf("<[%d]> = ", self->names[self->i].value_i);
                else
                    printf("%s = ", self->names[self->i].value_s);
                AWAIT_FN(obj_getitem, self->obj, self->names[self->i], 1, &self->obj1);
                AWAIT_FN(debug_print, self->obj1);
                printf("; ");
            }
            putchar('}');
        }
    }
} ASYNC_END()

object default_scope = {
    .vtable = &vt_empty,
};

ASYNC_DECLARE(eval_print_default_scope, ast_node* node)
ASYNC_DEFINE(eval_print_default_scope, ast_node* node, object* obj)
{
    AWAIT_FN(eval_node, self->node, &default_scope, &self->obj);
    AWAIT_FN(debug_print, self->obj);
} ASYNC_END()

void async_run(struct AsyncFrame* frame);

static void debug_print_sync(object* obj)
{
    async_run(FRAME_FN(debug_print, obj));
}

void repl(void)
{
    char* line = NULL;
    size_t sz = 0;
    ssize_t l = 0;
    while((printf("> "), l = getline(&line, &sz, stdin)) > 0)
    {
        jmp_buf* old_eeh = error_exit_handler;
        jmp_buf bf;
        if(setjmp(bf))
        {
            error_exit_handler = old_eeh;
            continue;
        }
        error_exit_handler = &bf;
        ast_node* node = lex_and_parse_string(line, "<stdin>");
        async_run(FRAME_FN(eval_print_default_scope, node));
        putchar('\n');
        error_exit_handler = old_eeh;
    }
    putchar('\n');
}

ASYNC_DECLARE(async_fork, pid_t* pid; int* status)
ASYNC_DECLARE(async_error_coop,)

ASYNC_DECLARE(traverse, object* obj)

ASYNC_DECLARE(traverse_item, object* obj; str_or_int key; object** ans)
ASYNC_DEFINE(traverse_item, object* obj; str_or_int key; object** ans,)
{
    AWAIT_FN(obj_getitem, self->obj, self->key, 1, &self->obj);
    if(self->ans)
        *self->ans = self->obj;
    if(self->obj)
        TAILCALL_FN(traverse, self->obj);
} ASYNC_END()

ASYNC_DEFINE(traverse, object* obj,
    int type;
    const str_or_int* names;
    struct lock* locks;
    struct lock tn;
    int i)
{
    AWAIT_FN(async_spawn, FRAME_FN(obj_getnames, self->obj, &self->names), &self->tn);
    AWAIT_FN(obj_gettype, self->obj, &self->type);
    if(self->type == TYPE_FUNC)
    {
        fprintf(stderr, "error: derivation() argument cannot contain a function\n");
        error_exit();
    }
    LOCK(&self->tn);
    UNLOCK(&self->tn);
    int len;
    for(len = 0; self->names[len].which != NONE; len++);
    self->locks = chk_calloc(sizeof(*self->locks) * len);
    for(self->i = 0; self->names[self->i].which != NONE; self->i++)
        AWAIT_FN(async_spawn, FRAME_FN(traverse_item, self->obj, self->names[self->i], NULL), self->locks+self->i);
    for(self->i = 0; self->names[self->i].which != NONE; self->i++)
    {
        LOCK(self->locks+self->i);
        UNLOCK(self->locks+self->i);
    }
    chk_free(self->locks, sizeof(*self->locks) * self->i);
} ASYNC_END()

ASYNC_DECLARE(obj_getitem_to_ht, object* obj; str_or_int key; struct hash_table* ht; struct hash_table* ht2)
ASYNC_DEFINE(obj_getitem_to_ht, object* obj; str_or_int key; struct hash_table* ht; struct hash_table* ht2,
    object* obj1;
    struct lock obj1_lock)
{
    AWAIT_FN(obj_getitem, self->obj, self->key, 1, &self->obj);
    if(!self->obj)
        RETURN();
    AWAIT_FN(obj_getitem, self->obj, MKSTR("needle"), 1, &self->obj1);
    if(self->obj1)
    {
        AWAIT_FN(async_spawn, FRAME_FN(obj_tostring_assert, self->obj1, &self->obj1), &self->obj1_lock);
        AWAIT_FN(obj_getitem, self->obj, MKSTR("haystack"), 1, &self->obj);
        if(!self->obj)
        {
            fprintf(stderr, "error: needle specified without haystack\n");
            error_exit();
        }
        AWAIT_FN(obj_tostring_assert, self->obj, &self->obj);
        LOCK(&self->obj1_lock);
        UNLOCK(&self->obj1_lock);
        hash_table_add(self->ht, self->key, (char*)((str_object*)self->obj)->value);
        hash_table_add(self->ht2, self->key, (char*)((str_object*)self->obj1)->value);
    }
    else
    {
        AWAIT_FN(obj_tostring_assert, self->obj, &self->obj);
        hash_table_add(self->ht, self->key, (char*)((str_object*)self->obj)->value);
    }
} ASYNC_END()

ASYNC_DECLARE(prepare_argv, object* obj; struct hash_table* out; struct hash_table* out2)
ASYNC_DEFINE(prepare_argv, object* obj; struct hash_table* out; struct hash_table* out2,
    const str_or_int* names;
    int len;
    int i;
    struct lock* locks)
{
    AWAIT_FN(obj_getnames, self->obj, &self->names);
    for(self->len = 0; self->names[self->len].which != NONE; self->len++);
    self->locks = chk_calloc(sizeof(*self->locks) * self->len);
    for(self->i = 0; self->i < self->len; self->i++)
        AWAIT_FN(async_spawn, FRAME_FN(obj_getitem_to_ht, self->obj, self->names[self->i], self->out, self->out2), self->locks+self->i);
    AWAIT_FN(traverse, self->obj);
    for(self->i = 0; self->i < self->len; self->i++)
    {
        LOCK(self->locks+self->i);
        UNLOCK(self->locks+self->i);
    }
    chk_free(self->locks, sizeof(*self->locks) * self->len);
} ASYNC_END()

static int sisort(const void* p1, const void* p2)
{
    str_or_int a = *(str_or_int*)p1;
    str_or_int b = *(str_or_int*)p2;
    if(a.which == NONE)
        return b.which == NONE ? 0 : -1;
    else if(b.which == NONE)
        return 1;
    else if(a.which == INT)
        return b.which == INT ? (b.value_i < a.value_i ? 1 : (b.value_i > a.value_i ? -1 : 0)) : -1;
    else if(b.which == INT)
        return 1;
    else
        return strcmp(a.value_s, b.value_s);
}

static const char* substitute_needle(const char* haystack, const char* needle, const char* out, const char* prefix, char q)
{
    if(!haystack)
        return NULL;
    if(!needle)
    {
        if(prefix)
        {
            int l1 = strlen(prefix);
            int l2 = strlen(haystack);
            char* out = chk_malloc(l1+l2+2);
            strcpy(out, prefix);
            out[l1] = q;
            strcpy(out+l1+1, haystack);
            return out;
        }
        return haystack;
    }
    int l = strlen(needle);
    int ll = strlen(haystack);
    int lo = strlen(out);
    int* kmp = chk_malloc(sizeof(*kmp) * l);
    kmp[0] = 0;
    for(int i = 1; i < l; i++)
    {
        int pl = kmp[i-1];
        while(pl > 0 && needle[pl] != needle[i])
            pl = kmp[pl-1];
        if(!pl && needle[0] != needle[i])
            kmp[i] = 0;
        else
            kmp[i] = pl + 1;
    }
    if(lo > l)
    {
        int j = 0, i;
        for(i = 0; haystack[i]; i++)
        {
            while(j < i && haystack[i] != needle[i-j])
                j = i - kmp[i-j-1];
            if(i == j + l)
            {
                ll += lo - l;
                j = i;
            }
            if(i == j && haystack[i] != *needle)
            {
                j++;
                continue;
            }
        }
        if(i == j + l)
            ll += lo - l;
    }
    if(prefix)
        ll += strlen(prefix) + 1;
    char* ans = chk_malloc(ll + 1);
    char* p = ans;
    if(prefix)
    {
        strcpy(p, prefix);
        p += strlen(p);
        *p++ = q;
    }
    int j = 0, i;
    for(i = 0; haystack[i]; i++)
    {
        while(j < i && haystack[i] != needle[i-j])
            j = i - kmp[i-j-1];
        if(i == j + l)
        {
            p -= l;
            strcpy(p, out);
            p += lo;
            j = i;
        }
        if(i == j && haystack[i] != *needle)
            j++;
        *p++ = haystack[i];
    }
    if(i == j + l)
    {
        p -= l;
        strcpy(p, out);
        p += lo;
    }
    *p++ = 0;
    chk_free(kmp, sizeof(*kmp) * l);
    return chk_realloc(ans, ll+1, p-ans);
}

static void feed_string_to_sha(struct sha256* sha, const char* s)
{
    if(!s)
    {
        sha256_feed(sha, "n", 1);
        return;
    }
    int l = strlen(s);
    char buf[16];
    sha256_feed(sha, buf, sprintf(buf, "%d ", l));
    sha256_feed(sha, s, l);
}

int makedirs(const char* path)
{
    struct stat st;
    if(!stat(path, &st))
        return 1; //target already exists
    char* slash = strrchr(path, '/');
    if(!slash)
        return 0;
    char* path2 = chk_malloc(slash - path + 1);
    memcpy(path2, path, slash - path);
    path2[slash - path] = 0;
    int parent = makedirs(path2);
    if(parent)
    {
        chk_free(path2, slash - path + 1);
        return parent > 0 ? 0 : -1;
    }
    int q = mkdir(path2, 0777);
    chk_free(path2, slash - path + 1);
    if(!q || errno == EEXIST)
        return 0;
    return -1;
}

void rmrf(const char* path)
{
    struct stat st;
    if(stat(path, &st))
        return;
    if(!S_ISDIR(st.st_mode))
    {
        unlink(path);
        return;
    }
    DIR* d = opendir(path);
    char** paths = NULL;
    int sz = 0;
    int cap = 0;
    struct dirent* de;
    int l0 = strlen(path);
    while((de = readdir(d)))
    {
        if(!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
            continue;
        if(sz == cap)
        {
            int old_cap = cap;
            if(cap == 0)
                cap = 1;
            else
                cap *= 2;
            paths = chk_realloc(paths, sizeof(*paths) * old_cap, sizeof(*paths) * cap);
        }
        int l1 = strlen(de->d_name);
        char* p = chk_malloc(l0+strlen(de->d_name)+2);
        memcpy(p, path, l0);
        p[l0] = '/';
        memcpy(p+l0+1, de->d_name, l1+1);
        paths[sz++] = p;
    }
    closedir(d);
    for(int i = 0; i < sz; i++)
    {
        rmrf(paths[i]);
        chk_free(paths[i], strlen(paths[i])+1);
    }
    chk_free(paths, sizeof(*paths) * cap);
}

ASYNC_DECLARE(hash_object, object* obj; struct sha256* sha)
ASYNC_DEFINE(hash_object, object* obj; struct sha256* sha,
    int type;
    const str_or_int* names;
    struct lock wait_names;
    object* conv_obj;
    int i)
{
    AWAIT_FN(async_spawn, FRAME_FN(obj_getnames, self->obj, &self->names), &self->wait_names);
    AWAIT_FN(obj_gettype, self->obj, &self->type);
    if(self->type == TYPE_FUNC)
    {
        fprintf(stderr, "error: derivation() argument cannot contain a function\n");
        error_exit();
    }
    LOCK(&self->wait_names);
    UNLOCK(&self->wait_names);
    char buf[32];
    if(self->type == TYPE_INT)
    {
        AWAIT_FN(obj_toint, self->obj, &self->conv_obj);
        sha256_feed(self->sha, buf, sprintf(buf, "i%d ", ((int_object*)self->conv_obj)->value));
    }
    else if(self->type == TYPE_STR)
    {
        AWAIT_FN(obj_tostring, self->obj, &self->conv_obj);
        sha256_feed(self->sha, "s", 1);
        feed_string_to_sha(self->sha, ((str_object*)self->conv_obj)->value);
        feed_string_to_sha(self->sha, ((str_object*)self->conv_obj)->shadow);
    }
    else
        sha256_feed(self->sha, "d", 1);
    for(self->i = 0; self->names[self->i].which != NONE; self->i++)
    {
        if(self->names[self->i].which == INT)
            sha256_feed(self->sha, buf, sprintf(buf, "i%d ", self->names[self->i].value_i));
        else
            feed_string_to_sha(self->sha, self->names[self->i].value_s);
        AWAIT_FN(obj_getitem, self->obj, self->names[self->i], 1, &self->conv_obj);
        AWAIT_FN(hash_object, self->conv_obj, self->sha);
    }
    sha256_feed(self->sha, "e", 1);
} ASYNC_END()

static struct hash_table drv_by_hash;
static struct hash_table drv_locks;

ASYNC_DECLARE(get_artifact_dir, object* args; const char** ans)
ASYNC_DEFINE(get_artifact_dir, object* args; const char** ans,)
{
    AWAIT_FN(obj_getitem, self->args, MKSTR("artifact_dir"), 1, &self->args);
    if(!self->args)
        *self->ans = NULL;
    else
    {
        AWAIT_FN(obj_tostring_assert, self->args, &self->args);
        *self->ans = ((str_object*)self->args)->value;
    }
} ASYNC_END()

/*
    derivation(args, **kwds)
*/
ASYNC_DECLARE(derivation_call, object* args; void* o; object** ans)
ASYNC_DEFINE(derivation_call, object* args; void* o; object** ans,
    object* args0;
    struct hash_table ht_stdout_redir;
    struct hash_table ht_stdout_redir_needle;
    object* arg1;
    const char* artifact_dir;
    struct lock stdout_redir_wait;
    struct lock arg1_wait;
    struct lock traverse_all_wait;
    struct lock artifact_dir_wait;
    struct hash_table haystacks;
    struct hash_table needles;
    str_or_int* keys;
    char** argv;
    char** envp;
    char* stdout_redir;
    pid_t pid;
    int status;
    char path[71];
    int stdout_fd;
    struct lock* drv_lock;
    int nargs;
    int nenv;
    const str_or_int* env_keys;
    char* path_alloc;
    struct sha256 sha)
{
    self->args0 = self->args;
    AWAIT_FN(async_spawn, FRAME_FN(obj_getitem_to_ht, self->args, MKSTR("stdout"), &self->ht_stdout_redir, &self->ht_stdout_redir_needle), &self->stdout_redir_wait);
    AWAIT_FN(async_spawn, FRAME_FN(get_artifact_dir, self->args, &self->artifact_dir), &self->artifact_dir_wait);
    AWAIT_FN(async_spawn, FRAME_FN(obj_getitem, self->args, MKINT(1), 0, &self->arg1), &self->arg1_wait);
    AWAIT_FN(obj_getitem, self->args, MKINT(0), 1, &self->args);
    if(!self->args)
    {
        fprintf(stderr, "error: derivation() required argument \"args\" not passed\n");
        error_exit();
    }
    AWAIT_FN(async_spawn, FRAME_FN(traverse, self->args), &self->traverse_all_wait);
    AWAIT_FN(prepare_argv, self->args, &self->haystacks, &self->needles);
    LOCK(&self->arg1_wait);
    UNLOCK(&self->arg1_wait);
    LOCK(&self->traverse_all_wait);
    UNLOCK(&self->traverse_all_wait);
    if(self->arg1)
    {
        fprintf(stderr, "error: too many arguments passed to derivation()\n");
        error_exit();
    }
    LOCK(&self->stdout_redir_wait);
    UNLOCK(&self->stdout_redir_wait);
    LOCK(&self->artifact_dir_wait);
    UNLOCK(&self->artifact_dir_wait);
    str_or_int* keys = chk_malloc(sizeof(*keys) * self->haystacks.size);
    int j = 0;
    for(int i = 0; i < self->haystacks.capacity; i++)
        for(struct hash_bucket* hb = self->haystacks.buckets[i]; hb; hb = hb->next)
            keys[j++] = hb->si;
    qsort(keys, j, sizeof(*keys), sisort);
    self->keys = keys;
    str_or_int* end_keys = keys + j;
    while(keys < end_keys && keys->which == NONE)
        keys++;
    while(keys < end_keys && keys->which == INT && keys->value_i < 0)
        keys++;
    int nargs = 0;
    while(keys < end_keys && keys->which == INT && keys->value_i == nargs)
    {
        keys++;
        nargs++;
    }
    while(keys < end_keys && keys->which == INT)
        keys++;
    int nenv = end_keys - keys;
    self->nargs = nargs;
    self->nenv = nenv;
    self->env_keys = keys;
    sha256_init(&self->sha);
    char buf[32];
    AWAIT_FN(hash_object, self->args0, &self->sha);
    sha256_close(&self->sha, buf);
    strcpy(self->path, "build/");
    for(int i = 0; i < 32; i++)
        sprintf(self->path+6+2*i, "%02hhx", (unsigned char)buf[i]);
    if((self->path_alloc = hash_table_get(&drv_by_hash, MKSTR(self->path))))
    {
        self->status = 0;
        goto exit;
    }
    self->drv_lock = hash_table_get(&drv_locks, MKSTR(self->path));
    if(self->drv_lock)
        LOCK(self->drv_lock);
    if((self->path_alloc = hash_table_get(&drv_by_hash, MKSTR(self->path))))
    {
        //must have yielded
        UNLOCK(self->drv_lock);
        self->drv_lock = NULL;
        self->status = 0;
        goto exit;
    }
    if(!self->artifact_dir)
    {
        self->path_alloc = chk_malloc(72);
        strcpy(self->path_alloc, self->path);
    }
    else
    {
        self->path_alloc = chk_malloc(strlen(self->artifact_dir)+66);
        strcpy(self->path_alloc, self->artifact_dir);
        memcpy(self->path_alloc+strlen(self->artifact_dir), self->path+5, 66);
    }
    if(!self->drv_lock)
    {
        self->drv_lock = NEW(struct lock, {0});
        hash_table_add(&drv_locks, MKSTR(self->path_alloc), self->drv_lock);
        LOCK(self->drv_lock);
    }
    self->argv = chk_malloc(sizeof(*self->argv) * (self->nargs + 1));
    for(int i = 0; i < self->nargs; i++)
        self->argv[i] = (char*)substitute_needle(
            hash_table_get(&self->haystacks, MKINT(i)),
            hash_table_get(&self->needles, MKINT(i)),
            self->path_alloc,
            NULL,
            0
        );
    self->argv[self->nargs] = NULL;
    self->envp = chk_malloc(sizeof(*self->envp) * (self->nenv + 1));
    for(int i = 0; i < self->nenv; i++)
        self->envp[i] = (char*)substitute_needle(
            hash_table_get(&self->haystacks, self->env_keys[i]),
            hash_table_get(&self->needles, self->env_keys[i]),
            self->path_alloc,
            self->env_keys[i].value_s,
            '='
        );
    self->envp[self->nenv] = NULL;
    self->stdout_redir = (char*)substitute_needle(
        hash_table_get(&self->ht_stdout_redir, MKSTR("stdout")),
        hash_table_get(&self->ht_stdout_redir_needle, MKSTR("stdout")),
        self->path_alloc,
        NULL,
        0
    );
    if(makedirs(self->path_alloc) == 1)
        goto exit; //already built
    if(self->stdout_redir)
    {
        self->stdout_fd = open(self->stdout_redir, O_WRONLY | O_CREAT | O_TRUNC, 0666);
        if(self->stdout_fd < 0)
        {
            fprintf(stderr, "error: failed to open \"%s\" for stdout redirect: %s\n", self->stdout_redir, strerror(errno));
            error_exit();
        }
    }
    else
        self->stdout_fd = -1;
    printf("+");
    for(int i = 0; i < self->nargs; i++)
        printf(" %s", self->argv[i]);
    if(self->stdout_redir)
        printf(" > %s", self->stdout_redir);
    printf("\n");
    AWAIT_FN(async_fork, &self->pid, &self->status);
    if(!self->pid)
    {
        for(int i = 0; self->envp[i]; i++)
            putenv(self->envp[i]);
        if(self->stdout_redir)
        {
            if(self->stdout_fd >= 0 && dup2(self->stdout_fd, 1) != 1)
            {
                if(write(self->status, &errno, sizeof(errno)) < 0)
                    _exit(1);
                _exit(1);
            }
            close(self->stdout_fd);
        }
        fcntl(self->status, F_SETFD, fcntl(self->status, F_GETFD) | FD_CLOEXEC);
        execvp(self->argv[0], self->argv);
        if(write(self->status, &errno, sizeof(errno)) < 0)
            _exit(1);
        _exit(1);
    }
    close(self->stdout_fd);
    if(self->status)
    {
        rmrf(self->path_alloc);
        fprintf(stderr, "error: builder process \"%s", self->argv[0]);
        for(int i = 1; self->argv[i]; i++)
            fprintf(stderr, " %s", self->argv[i]);
        fprintf(stderr, "\" ");
    }
    for(int i = 0; self->argv[i]; i++)
        if(self->argv[i] != hash_table_get(&self->haystacks, MKINT(i)))
            chk_free(self->argv[i], strlen(self->argv[i])+1);
    for(int i = 0; self->envp[i]; i++)
        chk_free(self->envp[i], strlen(self->envp[i])+1);
    if(self->stdout_redir && self->stdout_redir != hash_table_get(&self->ht_stdout_redir, MKSTR("stdout")))
        chk_free(self->stdout_redir, strlen(self->stdout_redir)+1);
exit:
    free_buckets(self->haystacks.buckets, self->haystacks.capacity);
    free_buckets(self->needles.buckets, self->needles.capacity);
    free_buckets(self->ht_stdout_redir.buckets, self->ht_stdout_redir.capacity);
    free_buckets(self->ht_stdout_redir_needle.buckets, self->ht_stdout_redir_needle.capacity);
    if(self->status)
        AWAIT_FN(async_error_coop,);
    hash_table_add(&drv_by_hash, MKSTR(self->path_alloc), self->path_alloc);
    *self->ans = link_cle(NEW(str_object, {
        .self = { .vtable = &vt_str },
        .value = self->path_alloc,
    }));
    if(self->drv_lock)
        UNLOCK(self->drv_lock);
} ASYNC_END()

syscall_object derivation_fn = {
    .self = { .vtable = &vt_syscall },
    .call = derivation_call,
    .opaque = NULL
};

void prune_path(char* path)
{
    char* path0 = path;
    if(*path == '/')
        path++;
    char* out = path;
    while(*path)
    {
        if(path[0] == '.' && (!path[1] || path[1] == '/'))
            path++;
        if(path[0] == '.' && path[1] == '.' && (!path[2] || path[2] == '/'))
        {
            if(out != path0)
            {
                out--;
                if(out != path0 && *out == '/')
                    out--;
                while(out != path0 && *out != '/')
                    out--;
                if(out == path0 && *out == '/')
                    out++;
            }
            path += 2;
        }
        if(*path == '/')
        {
            if(out == path0 || out[-1] != '/')
                *out++ = '/';
            path++;
        }
        else if(*path)
            *out++ = *path++;
    }
    while(out != path0 && out[-1] == '/')
        out--;
    if(out == path0)
        *out++ = '/';
    *out++ = 0;
}

char* dirname(const char* path)
{
    int l = strlen(path);
    char* out = chk_malloc(l+1);
    memcpy(out, path, l+1);
    char* p = out + l;
    while(p != out && *p != '/')
        p--;
    if(*p == '/')
        *p = 0;
    return out;
}

const char* get_base_path(void)
{
    static const char* ans;
    if(ans)
        return ans;
    int sz = 64;
    char* buf = chk_malloc(64);
    for(;;)
    {
        char* buf2 = getcwd(buf, sz);
        if(!buf2)
        {
            if(errno == ERANGE)
            {
                buf = chk_realloc(buf, sz, sz*2);
                sz *= 2;
                continue;
            }
            free(buf);
            fprintf(stderr, "error: failed to get the current working directory\n");
            error_exit();
        }
        return ans = buf2;
    }
}

ASYNC_DECLARE(source_call, object* args; void* opaque; object** ans)
ASYNC_DECLARE(path_call, object* args; void* opaque; object** ans)

ASYNC_DECLARE(import_call, object* args; void* opaque; object** ans)
ASYNC_DEFINE(import_call, object* args; void* opaque; object** ans, object* arg0)
{
    AWAIT_FN(obj_getitem, self->args, MKINT(0), 1, &self->arg0);
    if(!self->arg0)
    {
        fprintf(stderr, "error: import() expects at least 1 argument\n");
        error_exit();
    }
    AWAIT_FN(obj_tostring_assert, self->arg0, &self->arg0);
    const char* path = ((str_object*)self->arg0)->value;
    const char* base_path = (*path == '/') ? "/" : (self->opaque ? self->opaque : get_base_path());
    int l1 = strlen(path);
    int l2 = strlen(base_path);
    char* tgt_path = chk_malloc(l1+l2+2);
    memcpy(tgt_path, base_path, l2);
    tgt_path[l2] = '/';
    memcpy(tgt_path+l2+1, path, l1+1);
    prune_path(tgt_path);
    FILE* f = fopen(tgt_path, "r");
    if(!f)
    {
        fprintf(stderr, "error: cannot open source file at %s\n", tgt_path);
        chk_free(tgt_path, l1+l2+2);
        error_exit();
    }
    ast_node* node;
    jmp_buf bf;
    jmp_buf* old_eeh = error_exit_handler;
    if(setjmp(bf))
    {
        node = NULL;
        error_exit_handler = old_eeh;
    }
    else
    {
        error_exit_handler = &bf;
        node = lex_and_parse_file(f, tgt_path);
        error_exit_handler = old_eeh;
    }
    fclose(f);
    if(!node)
        error_exit();
    object* import_for_them = link_cle(NEW(syscall_object, {
        .self = { .vtable = &vt_syscall },
        .call = import_call,
        .opaque = dirname(tgt_path)
    }));
    object* source_for_them = link_cle(NEW(syscall_object, {
        .self = { .vtable = &vt_syscall },
        .call = source_call,
        .opaque = dirname(tgt_path)
    }));
    object* path_for_them = link_cle(NEW(syscall_object, {
        .self = { .vtable = &vt_syscall },
        .call = path_call,
        .opaque = dirname(tgt_path)
    }));
    str_or_int* scope_overlay_names = chk_malloc(sizeof(*scope_overlay_names)*4);
    scope_overlay_names[0] = MKSTR("import");
    scope_overlay_names[1] = MKSTR("path");
    scope_overlay_names[2] = MKSTR("source");
    scope_overlay_names[3] = MKNONE();
    object* scope_overlay = NEW(object, {
        .vtable = &vt_empty,
        .cached_names = scope_overlay_names
    });
    hash_table_add(&scope_overlay->cached_props, MKSTR("import"), import_for_them);
    hash_table_add(&scope_overlay->cached_props, MKSTR("path"), path_for_them);
    hash_table_add(&scope_overlay->cached_props, MKSTR("source"), source_for_them);
    AWAIT_FN(eval_node, node, make_overlay(&default_scope, scope_overlay), self->ans);
    self->args = link_cle(NEW(recur_object, {
        .self = { .vtable = &vt_shift },
        .backing = self->args
    }));
    TAILCALL_FN(obj_call, *self->ans, self->args, self->ans, NULL);
} ASYNC_END()

syscall_object import_fn = {
    .self = { .vtable = &vt_syscall },
    .call = import_call,
    .opaque = NULL
};

ASYNC_DECLARE(source_or_path_call, object* args; void* opaque; int is_path; object** ans)
ASYNC_DEFINE(source_or_path_call, object* args; void* opaque; int is_path; object** ans,
    object* arg0;
    object* arg1;
    struct lock arg1_wait)
{
    AWAIT_FN(async_spawn, FRAME_FN(obj_getitem, self->args, MKINT(1), 0, &self->arg1), &self->arg1_wait);
    AWAIT_FN(obj_getitem, self->args, MKINT(0), 1, &self->arg0);
    if(self->arg0)
        AWAIT_FN(obj_tostring_assert, self->arg0, &self->arg0);
    LOCK(&self->arg1_wait);
    UNLOCK(&self->arg1_wait);
    if(!self->arg0 || self->arg1)
    {
        fprintf(stderr, "error: %s() expects exactly 1 argument\n", self->is_path?"path":"source");
        error_exit();
    }
    const char* path = ((str_object*)self->arg0)->value;
    const char* base_path = (*path == '/') ? "/" : (self->opaque ? self->opaque : get_base_path());
    int l1 = strlen(path);
    int l2 = strlen(base_path);
    char* tgt_path = chk_malloc(l1+l2+2);
    memcpy(tgt_path, base_path, l2);
    tgt_path[l2] = '/';
    memcpy(tgt_path+l2+1, path, l1+1);
    prune_path(tgt_path);
    char* hash_alloc = NULL;
    if(!self->is_path)
    {
        FILE* f = fopen(tgt_path, "rb");
        if(!f)
        {
            fprintf(stderr, "error: failed to open \"%s\"\n", tgt_path);
            chk_free(tgt_path, l1+l2+2);
            error_exit();
        }
        struct sha256 sha;
        sha256_init(&sha);
        sha256_feed(&sha, "S", 1);
        char c[1024];
        ssize_t cnt;
        while((cnt = fread(c, 1, 1024, f)) > 0)
            sha256_feed(&sha, c, cnt);
        if(!feof(f))
        {
            fclose(f);
            fprintf(stderr, "error: file read error\n");
            chk_free(tgt_path, l1+l2+2);
            error_exit();
        }
        uint8_t hash[32];
        sha256_close(&sha, hash);
        char buf[65];
        for(int i = 0; i < 32; i++) 
            sprintf(buf+2*i, "%02x", hash[i]);
        if(!(hash_alloc = hash_table_get(&drv_by_hash, MKSTR(buf))))
        {
            hash_alloc = chk_malloc(65);
            memcpy(hash_alloc, buf, 65);
            hash_table_add(&drv_by_hash, MKSTR(hash_alloc), hash_alloc);
        }
    }
    *self->ans = NEW(str_object, {
        .self = {
            .vtable = &vt_str,
        },
        .value = tgt_path,
        .shadow = hash_alloc
    });
} ASYNC_END()

ASYNC_DEFINE(source_call, object* args; void* opaque; object** ans,)
{
    TAILCALL_FN(source_or_path_call, self->args, self->opaque, 0, self->ans);
} ASYNC_END()

syscall_object source_fn = {
    .self = { .vtable = &vt_syscall },
    .call = source_call,
    .opaque = NULL
};

ASYNC_DEFINE(path_call, object* args; void* opaque; object** ans,)
{
    TAILCALL_FN(source_or_path_call, self->args, self->opaque, 1, self->ans);
} ASYNC_END()

syscall_object path_fn = {
    .self = { .vtable = &vt_syscall },
    .call = path_call,
    .opaque = NULL
};

ASYNC_DECLARE(read_file_call, object* args; void* opaque; object** ans)
ASYNC_DEFINE(read_file_call, object* args; void* opaque; object** ans, const str_or_int* args_keys)
{
    AWAIT_FN(obj_getnames, self->args, &self->args_keys);
    if(sicmp(self->args_keys[0], MKINT(0))
    || sicmp(self->args_keys[1], MKNONE()))
    {
        fprintf(stderr, "error: read_file() expects one positional argument\n");
        error_exit();
    }
    AWAIT_FN(obj_getitem, self->args, MKINT(0), 1, &self->args);
    AWAIT_FN(obj_tostring_assert, self->args, &self->args);
    void* path_alloc;
    int path_size;
    const char* path = ((str_object*)self->args)->value;
    FILE* f = fopen(path, "rb");
    if(!f)
    {
        fprintf(stderr, "error: failed to open \"%s\"\n", path);
        error_exit();
    }
    fseek(f, 0, SEEK_END);
    size_t len = ftell(f);
    if(len != (int)len)
    {
        fclose(f);
        fprintf(stderr, "error: file too long\n");
        error_exit();
    }
    fseek(f, 0, SEEK_SET);
    char* ans = chk_malloc(len+1);
    char* p = ans;
    char* pend = ans + len;
    while(pend > p)
    {
        ssize_t chk = fread(p, 1, pend-p, f);
        if(chk <= 0)
        {
            fclose(f);
            fprintf(stderr, "error: file read error\n");
            error_exit();
        }
        p += chk;
    }
    *p++ = 0;
    *self->ans = NEW(str_object, {
        .self = {
            .vtable = &vt_str,
        },
        .value = ans
    });
} ASYNC_END()

syscall_object read_file_fn = {
    .self = { .vtable = &vt_syscall },
    .call = read_file_call,
    .opaque = NULL
};

ASYNC_DECLARE(error_resolve_arg1_to_string, object* args; object** ans)
ASYNC_DEFINE(error_resolve_arg1_to_string, object* args; object** ans,)
{
    AWAIT_FN(obj_getitem, self->args, MKINT(0), 1, self->ans);
    TAILCALL_FN(obj_tostring_assert, *self->ans, self->ans);
} ASYNC_END()

ASYNC_DECLARE(error_call, object* args; void* opaque; object** ans)
ASYNC_DEFINE(error_call, object* args; void* opaque; object** ans,
    object* arg1;
    object* arg2;
    struct lock arg1_lock;
    const str_or_int* args_keys;
)
{
    AWAIT_FN(async_spawn, FRAME_FN(error_resolve_arg1_to_string, self->args, &self->arg1), &self->arg1_lock);
    AWAIT_FN(obj_getnames, self->args, &self->args_keys);
    if(sicmp(self->args_keys[0], MKINT(0))
    || sicmp(self->args_keys[1], MKNONE()))
    {
        fprintf(stderr, "error: error() expects one positional argument\n");
        error_exit();
    }
    LOCK(&self->arg1_lock);
    UNLOCK(&self->arg1_lock);
    fprintf(stderr, "error: %s\n", ((str_object*)self->arg1)->value);
    error_exit();
} ASYNC_END()

syscall_object error_fn = {
    .self = { .vtable = &vt_syscall },
    .call = error_call,
    .opaque = NULL
};

ASYNC_DECLARE(do_chr, object* args; object** ans)
ASYNC_DEFINE(do_chr, object* args; object** ans,
    int type;
)
{
    AWAIT_FN(obj_getitem, self->args, MKINT(0), 1, self->ans);
    AWAIT_FN(obj_gettype, *self->ans, &self->type);
    if(self->type != TYPE_INT)
    {
        fprintf(stderr, "error: chr() argument must be an integer\n");
        error_exit();
    }
    AWAIT_FN(obj_toint_assert, *self->ans, self->ans);
    int val = ((int_object*)*self->ans)->value;
    if(val <= 0 || val >= 256)
    {
        fprintf(stderr, "error: chr() argument must be in range 1-255, got %d\n", val);
        error_exit();
    }
    *self->ans = NEW(char_object, {
        .self = { .vtable = &vt_char, },
        .c = (char)val,
    });
} ASYNC_END()

ASYNC_DECLARE(chr_call, object* args; void* opaque; object** ans)
ASYNC_DEFINE(chr_call, object* args; void* opaque; object** ans,
    object* arg1;
    object* arg2;
    struct lock ans_lock;
    const str_or_int* args_keys;
)
{
    AWAIT_FN(async_spawn, FRAME_FN(do_chr, self->args, self->ans), &self->ans_lock);
    AWAIT_FN(obj_getnames, self->args, &self->args_keys);
    if(sicmp(self->args_keys[0], MKINT(0))
    || sicmp(self->args_keys[1], MKNONE()))
    {
        fprintf(stderr, "error: chr() expects one positional argument\n");
        error_exit();
    }
    LOCK(&self->ans_lock);
    UNLOCK(&self->ans_lock);
} ASYNC_END()

syscall_object chr_fn = {
    .self = { .vtable = &vt_syscall, },
    .call = chr_call,
    .opaque = NULL,
};

struct fork_opaque
{
    pid_t* pid;
    int* status;
};

ASYNC_DEFINE(async_fork, pid_t* pid; int* status, struct fork_opaque op)
{
    self->op.pid = self->pid;
    self->op.status = self->status;
    AWAIT_FN(AsyncYield, 1, &self->op);
    AWAIT_FN(AsyncYield, 2, &self->op);
    if(*self->pid != 0)
        AWAIT_FN(AsyncYield, 3, &self->op);
} ASYNC_END()

ASYNC_DECLARE(slow_unlock_for_async_spawn, struct lock* lock; struct AsyncFrame* frame)
ASYNC_DEFINE(slow_unlock_for_async_spawn, struct lock* lock; struct AsyncFrame* frame,)
{
    TAILCALL_FN(slow_unlock, self->lock);
} ASYNC_END()

ASYNC_DEFINE(async_spawn, struct AsyncFrame* frame; struct lock* lock,)
{
    LOCK(self->lock);
    self->frame->next = FRAME_FN(slow_unlock_for_async_spawn, self->lock, Self->next);
    AWAIT_FN(AsyncYield, 4, self->frame);
} ASYNC_END()

ASYNC_DEFINE(slow_lock, struct lock* lock,)
{
    AWAIT_FN(AsyncYield, 5, self->lock);
} ASYNC_END()

ASYNC_DEFINE(slow_unlock, struct lock* lock,)
{
    self->lock->locked = 0;
    struct lock_ll* waiters = self->lock->waiters;
    self->lock->waiters = NULL;
    if(waiters)
        AWAIT_FN(AsyncYield, 6, waiters);
} ASYNC_END()

ASYNC_DEFINE(async_error_coop,,)
{
    AWAIT_FN(AsyncYield, 7, NULL);
} ASYNC_END()

void async_run(struct AsyncFrame* frame)
{
    struct hash_table waiters = {0};
    int ncpus = 4;
    int njobs = 0;
    struct lock_ll* volatile job_queue = NULL;
    struct lock_ll* volatile thread_queue = NEW(struct lock_ll, {frame});
    volatile int have_errors = 0;
    volatile int locks = 0;
#define SET_ERROR() do\
{\
    if(!have_errors && njobs)\
        fprintf(stderr, "note: waiting for unfinished jobs...\n");\
    have_errors = 1;\
}\
while(0)
    jmp_buf* old_eeh = error_exit_handler;
    jmp_buf eeh;
    if(setjmp(eeh))
        SET_ERROR();
    error_exit_handler = &eeh;
    while((thread_queue && !have_errors) || (job_queue && !have_errors) || njobs)
    {
        while(thread_queue && !have_errors)
        {
            int request;
            void* payload;
            struct lock_ll* tmp = thread_queue;
            thread_queue = tmp->next;
            tmp->waiter = AsyncRunFrame(tmp->waiter, &request, &payload);
            if(!tmp->waiter)
            {
                chk_free(tmp, sizeof(tmp));
                continue;
            }
            if(request == 1) // fork
            {
                struct fork_opaque* fo = payload;
                tmp->next = job_queue;
                job_queue = tmp;
            }
            else if(request == 4) // spawn
            {
                thread_queue = NEW(struct lock_ll, {payload, thread_queue});
                tmp->next = thread_queue;
                thread_queue = tmp;
            }
            else if(request == 5) // slow_lock
            {
                locks++;
                struct lock* the_lock = payload;
                tmp->next = the_lock->waiters;
                the_lock->waiters = tmp;
            }
            else if(request == 6) // slow_unlock
            {
                struct lock_ll* waiters = payload;
                struct lock_ll* i = waiters;
                while(i->next)
                {
                    i = i->next;
                    locks--;
                }
                locks--;
                i->next = thread_queue;
                thread_queue = waiters;
                tmp->next = thread_queue;
                thread_queue = tmp;
            }
            else
                abort();
        }
        while(!have_errors && job_queue && njobs < ncpus)
        {
            int request;
            void* payload;
            struct lock_ll* tmp = job_queue;
            job_queue = tmp->next;
            tmp->waiter = AsyncRunFrame(tmp->waiter, &request, &payload);
            if(!tmp->waiter)
                abort();
            if(request != 2) // fork continue
                abort();
            struct fork_opaque* fo = payload;
            int p[2];
            if(pipe(p))
            {
                fprintf(stderr, "error: pipe() failed\n");
                AsyncCancel(tmp->waiter);
                chk_free(tmp, sizeof(tmp));
                SET_ERROR();
                continue;
            }
            pid_t pid = fork();
            if(pid < 0)
            {
                fprintf(stderr, "error: fork() failed\n");
                AsyncCancel(tmp->waiter);
                chk_free(tmp, sizeof(tmp));
                SET_ERROR();
                continue;
            }
            *fo->pid = pid;
            if(pid == 0)
            {
                *fo->status = p[1];
                close(p[0]);
                AsyncRunFrame(tmp->waiter, &request, &payload);
                //should have exited
                abort();
            }
            *fo->status = p[0];
            close(p[1]);
            hash_table_add(&waiters, MKINT(pid), tmp);
            njobs++;
        }
        while(njobs)
        {
            int status;
            pid_t pid = wait(&status);
            if(pid < 0)
            {
                fprintf(stderr, "error: njobs > 0, but wait() reports no processes\n");
                SET_ERROR();
                njobs = 0;
                continue;
            }
            njobs--;
            struct lock_ll* job = hash_table_get(&waiters, MKINT(pid));
            if(!job)
                //we're probably a subreaper for some reason. ignore the error
                continue;
            int request;
            void* payload;
            hash_table_add(&waiters, MKINT(pid), NULL);
            job->waiter = AsyncRunFrame(job->waiter, &request, &payload);
            if(request != 3) // fork end
                abort();
            struct fork_opaque* fo = payload;
            int status_fd = *fo->status;
            int error = 0;
            if(read(status_fd, &error, sizeof(error)) < 0)
                error = EPIPE;
            close(status_fd);
            if(error != 0)
                *fo->status = 1;
            *fo->status = status;
            if(status)
            {
                job->waiter = AsyncRunFrame(job->waiter, &request, &payload);
                if(request != 7) // error cooperation
                    abort();
                if(error != 0)
                    fprintf(stderr, "failed to start: %s\n", strerror(error));
                else if(WIFSIGNALED(status))
                {
                    int which = WTERMSIG(status);
                    fprintf(stderr, "terminated with signal %d (%s)\n", which, strsignal(which));
                }
                else if(WIFEXITED(status))
                {
                    int status = WEXITSTATUS(status);
                    fprintf(stderr, "exited with non-zero exit code %d\n", status);
                }
                else
                    fprintf(stderr, "terminated in an unknown way. Treating this as a failure.\n");
                AsyncCancel(job->waiter);
                chk_free(job, sizeof(job));
                SET_ERROR();
                continue;
            }
            job->next = thread_queue;
            thread_queue = job;
        }
    }
    if(locks && !have_errors)
    {
        fprintf(stderr, "error: deadlock on a mutex\n");
        have_errors = 1;
    }
#undef SET_ERROR
    error_exit_handler = old_eeh;
    while(job_queue)
    {
        AsyncCancel(job_queue->waiter);
        struct lock_ll* next = job_queue->next;
        chk_free(job_queue, sizeof(job_queue));
        job_queue = next;
    }
    free_buckets(waiters.buckets, waiters.capacity);
    if(have_errors)
        error_exit();
}

const char* std_src = NULL;

#ifndef STAGE1

int main(int argc, const char** argv)
{
    setbuf(stdout, NULL);
    hash_table_add(&default_scope.cached_props, MKSTR("chr"), &chr_fn);
    hash_table_add(&default_scope.cached_props, MKSTR("derivation"), &derivation_fn);
    hash_table_add(&default_scope.cached_props, MKSTR("error"), &error_fn);
    hash_table_add(&default_scope.cached_props, MKSTR("import"), &import_fn);
    hash_table_add(&default_scope.cached_props, MKSTR("path"), &path_fn);
    hash_table_add(&default_scope.cached_props, MKSTR("read_file"), &read_file_fn); //TODO: should this be overridden in imports?
    hash_table_add(&default_scope.cached_props, MKSTR("source"), &source_fn);
    hash_table_add(&default_scope.cached_props, MKSTR("std"), get_proxy(lex_and_parse_string(std_src, "<std>"), &default_scope));
    static str_or_int names[] = {
        MKSTR("chr"),
        MKSTR("derivation"),
        MKSTR("error"),
        MKSTR("import"),
        MKSTR("path"),
        MKSTR("read_file"),
        MKSTR("source"),
        MKSTR("std"),
        MKNONE()
    };
    default_scope.cached_names = names;
    const char* path0 = get_base_path();
    int l0 = strlen(path0);
    char* default_fb_path_buf = chk_malloc(l0+12);
    memcpy(default_fb_path_buf, path0, l0);
    memcpy(default_fb_path_buf+l0, "/default.fb", 12);
    const char* default_fb_path = default_fb_path_buf;
    if(argc == 2 && !strcmp(argv[1], "--repl"))
        repl();
    else
    {
        if(argc > 1 && !strcmp(argv[1], "-f"))
        {
            default_fb_path = argv[2];
            if(argc == 2)
                goto usage;
            argc -= 2;
            argv += 2;
        }
        if(argc <= 2)
        {
            const char* target = argc == 1 ? "all" : argv[1];
            if(target[0] == '-')
                goto usage;
            FILE* f = fopen(default_fb_path, "r");
            if(!f)
            {
                fprintf(stderr, "error: file \"%s\" not found\n", default_fb_path);
                return 1;
            }
            jmp_buf buf;
            if(setjmp(buf))
            {
                fclose(f);
                return 1;
            }
            error_exit_handler = &buf;
            ast_node* ast = lex_and_parse_file(f, default_fb_path);
            fclose(f);
            error_exit_handler = NULL;
            object* root = get_proxy(ast, &default_scope);
            unrecur_object* unrecurred_root = (unrecur_object*)link_cle(NEW(unrecur_object, {
                .self = { .vtable = &vt_unrecur, },
                .orig = root,
            }));
            unrecurred_root->scope = (object*)unrecurred_root;
            object* deriv;
            async_run(FRAME_FN(traverse_item, (object*)unrecurred_root, MKSTR(target), &deriv));
            if(!deriv)
            {
                fprintf(stderr, "error: target \"%s\" not declared\n", target);
                return 1;
            }
        }
        else
        {
        usage:
            fprintf(stderr, "usage: fbuild [-f <qbfile>] [target] OR fbuild --repl\n");
            return 1;
        }
    }
    return 0;
}

#else
#include <sys/auxv.h>

//temporary executable to bundle std
int main(int argc, const char** argv)
{
    char* exe = (char*)getauxval(AT_EXECFN);
    char* path;
    if(argv[1] && !strcmp(argv[1], "--stage1"))
        path = argv[2];
    else
    {
        path = strdup(exe);
        if(strlen(path) <= 4 || strcmp(path + strlen(path) - 4, ".bin"))
        {
            fprintf(stderr, "error: stage1 must have .bin extension\n");
            return 1;
        }
        path[strlen(path) - 4] = 0;
    }
    FILE* f = fopen(path, "r");
    char* line = NULL;
    size_t sz = 0;
    int have_std_src = 0;
    while(getline(&line, &sz, f) > 0 && strcmp(line, "#if 0 //std\n"))
        if(!strcmp(line, "const char* std_src = NULL;\n"))
            have_std_src = 1;
    if(strcmp(line, "#if 0 //std\n"))
    {
        fprintf(stderr, "error: std not found\n");
        return 1;
    }
    if(!have_std_src)
    {
        fprintf(stderr, "error: std_src not found\n");
        return 1;
    }
    FILE* wr;
    pid_t pid;
    if(!argv[1] || strcmp(argv[1], "--stage1"))
    {
        int p[2];
        if(pipe(p))
        {
            perror("error: pipe() failed");
            return 1;
        }
        printf("+ cc -DSTAGE2 -x c - -o %s.bin\n", path);
        pid = fork();
        if(!pid)
        {
            if(dup2(p[0], 0) != 0)
                _exit(1);
            close(p[1]);
            execlp("cc", "cc", "-DSTAGE2", "-x", "c", "-", "-o", exe, NULL);
            _exit(1);
        }
        close(p[0]);
        wr = fdopen(p[1], "w");
    }
    else
        wr = stdout;
    FILE* f2 = fopen(path, "r");
    while(getline(&line, &sz, f2) > 0)
    {
        if(!strcmp(line, "const char* std_src = NULL;\n"))
        {
            fprintf(wr, "const char* std_src = \"");
            while(getline(&line, &sz, f) > 0 && strcmp(line, "#endif\n"))
            {
                size_t l = strlen(line);
                line[--l] = 0;
                size_t nquotes = 0;
                for(size_t i = 0; i < l; i++)
                    if(line[i] == '"')
                        nquotes++;
                size_t sz1 = sz;
                while(sz1 <= l + nquotes)
                    sz1 *= 2;
                if(sz1 != sz)
                {
                    sz = sz1;
                    line = realloc(line, sz);
                }
                size_t j = l + nquotes;
                line[j] = 0;
                for(size_t i = l - 1; i + 1; i--)
                {
                    line[--j] = line[i];
                    if(line[i] == '"')
                        line[--j] = '\\';
                }
                fprintf(wr, "%s\\n", line);
            }
            fprintf(wr, "\";\n");
            fclose(f);
            break;
        }
        fprintf(wr, "%s", line);
    }
    while(getline(&line, &sz, f2) > 0)
        fprintf(wr, "%s", line);
    fclose(f2);
    if(!argv[1] || strcmp(argv[1], "--stage1"))
    {
        fclose(wr);
        int status = 0;
        if(wait(&status) != pid || status != 0)
        {
            fprintf(stderr, "error: stage2 failed to compile\n");
            return 1;
        }
        execv(exe, (char**)argv);
        fprintf(stderr, "error: execv(stage2) failed\n");
        return 1;
    }
    return 0;
}
#endif

#if 0 //std
let
    std = '{
        instantiate = (x) => x() // { self = x; };
        map = let
            map0 = (f, x, i=0) => (i == #x) ? [] : [f(x[i])] ++ map0(f, x, i+1);
        in (f, x) => map0(f, x);
        endswith = let
            strcmp = (s1, i, s2, j) => (i == #s1 && j == #s2) || (s1[i] == s2[j] && strcmp(s1, i+1, s2, j+1));
        in (a, b) => (#a < #b) ? 0 : strcmp(a, #a-#b, b, 0);
        any = let
            any0 = (x, i=0) => (i == #x) ? 0 : x[i] || any0(x, i+1);
        in (x) => any0(x);
        all = let
            all0 = (x, i=0) => (i == #x) ? 1 : x[i] && all0(x, i+1);
        in (x) => all0(x);
        concat = let
            concat0 = (x, i=0) => (i == #x) ? [] : x[i] ++ concat0(x, i+1);
        in (x) => concat0(x);
        string_slice = (s, x, y) => (x >= y) ? "" : (x + 1 == y ? chr(s[x]) : string_slice(s, x, x+(y-x)/2) .. string_slice(s, x+(y-x)/2, y));
        isspace = (c) => c == 9 || c == 10 || c == 32;
        isspace_s = (s) => all(map(isspace, s));
        trim = let
            count_spaces_left = (s, x) => (x == #s || !isspace(s[x])) ? x : count_spaces_left(s, x+1);
            count_spaces_right = (s, x) => (x == 0 || !isspace(s[x-1])) ? x : count_spaces_right(s, x-1);
        in (s) => string_slice(s, count_spaces_left(s, 0), count_spaces_right(s, #s));
        split = let
            find_space = (s, x = 0) => x == #s ? -1 : (isspace(s[x]) ? x : find_space(s, x+1));
        in (s) => let
            t = trim(s);
            sp = find_space(t);
        in (sp == -1) ? (#t ? [t] : []) : ([string_slice(t, 0, sp)] ++ split(string_slice(t, sp+1, #t)));
        make_out = (left, mid, right) => { haystack = left..mid..right; needle = mid; };
        OUT = make_out("", "out", "");
        simple_compiler = '{
            detect = (src) => any(map((ext) => endswith(src, "." .. ext), extensions));
            compile = (src, add_opts=[]) => derivation([command] ++ pre_options ++ [src, "-c", "-o", OUT] ++ options ++ add_opts, deps=headers);
            pre_options = [];
            options = [];
            headers = [];
        };
        c_compiler = simple_compiler // '{ command = "cc"; extensions = ["c"]; };
        cxx_compiler = simple_compiler // '{ command = "c++"; extensions = ["cpp", "cxx", "c++"]; };
        languages = {
            c = c_compiler;
            cxx = cxx_compiler;
        };
        simple_linker = '{
            link = (objs, add_opts=[]) => derivation([command] ++ objs ++ ["-o", OUT] ++ options ++ add_opts);
            options = [];
            options_exe = [];
            options_lib = ["-fPIE", "-fPIC", "-shared"];
        };
        c_linker = simple_linker // '{ command = "cc"; };
        toolchain_base = '{
            compile_one = let
                h = headers;
                cplrs = map((x) => (x // '({ headers = h; }))(), compilers);
                ncplrs = #cplrs;
                compile0 = (src, opts, i=0) => i == ncplrs ? error("no compilers can handle "..src) : (cplrs[i].detect(src) ? cplrs[i].compile(src, opts) : compile0(src, opts, i+1));
            in (src, add_opts) => compile0(src, cflags++add_opts);
            link = let l = ldflags; in (linker // '({ options = linker().options ++ l; }))().link;
            build = (srcs, *, cflags=[], ldflags=[]) => link(
                map((x) => compile_one(x, cflags), srcs),
                ldflags
            );
            make_builder = (addflags) => (srcs, *, cflags=[], ldflags=[], libs=[]) => let
                cf = cflags;
                lf = addflags ++ ldflags;
                ls = libs;
            in build(
                srcs,
                cflags=cf++concat(map((x) => x.cflags, libs)),
                ldflags=lf++concat(map((x) => x.ldflags, libs)));
            executable = make_builder(linker().options_exe);
            library = make_builder(linker().options_lib);
            override = let
                h = headers;
                cf = cflags;
                lf = ldflags;
            in (headers=[], cflags=[], ldflags=[]) => let
                hdrs2 = headers;
                cf2 = cflags;
                ldf2 = ldflags;
            in toolchain(compilers, linker, headers=h++hdrs2, cflags=cf++cf2, ldflags=lf++ldf2);
        };
        toolchain = (compilers, linker, *, headers=[], cflags=[], ldflags=[]) => let
            cplrs = compilers;
            lnkr = linker;
            h = headers;
            cf = cflags;
            lf = ldflags;
        in instantiate(toolchain_base // '({
            compilers = cplrs;
            linker = lnkr;
            headers = h;
            cflags = cf;
            ldflags = lf;
        }));
        c_toolchain = toolchain([c_compiler], c_linker);
        cxx_toolchain = toolchain([cxx_compiler], c_linker, ldflags=["-lm", "-lstdc++"]);
        c_cxx_toolchain = toolchain([c_compiler, cxx_compiler], c_linker, ldflags=["-lm", "-lstdc++"]);
        target = (path, file, mode="755") => {
            all = [file];
            install = (prefix, d=[]) => derivation(["install", "-T", "-m", mode, file, prefix.."/"..path], deps=d);
            uninstall = (prefix, d=[]) => derivation(["rm", "-f", prefix.."/"..path], deps=d);
        };
        project = (prefix, binaries, libraries=[], assets=[]) => let
            mkdir = (x) => derivation(["mkdir", "-p", prefix.."/"..x]);
        in {
            all = map((x) => x.all, binaries ++ libraries ++ assets);
            install = map((x) => x.install(prefix.."/bin", [mkdir("bin")]), binaries)
                   ++ map((x) => x.install(prefix.."/lib", [mkdir("lib")]), libraries)
                   ++ map((x) => x.install(prefix.."/share", [mkdir("share")]), assets);
            uninstall = map((x) => x.uninstall(prefix.."/bin"), binaries)
                     ++ map((x) => x.uninstall(prefix.."/lib"), libraries)
                     ++ map((x) => x.uninstall(prefix.."/share"), assets);
        };
        project_ex = (targets, prefix) => {
            all = map((x) => x.all, targets);
            install = map((x) => x.install(prefix), targets);
            uninstall = map((x) => x.uninstall(prefix), targets);
        };
    };
in std().instantiate(std)
#endif
