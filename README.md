# fbuild

This is a WIP pure-functional build system for projects, heavily inspired by Nix.

## Language

The build system features a functional language, described in detail in [language.md](https://gitlab.com/sleirsgoevy/fbuild/-/blob/master/language.md).

## Built-in functions

* `import(path, **args)`. This function loads a function from an fbuild-language file at `path` (relative to the current file) and runs it with the provided arguments. Both positional and keyword arguments are fully supported.
* `derivation(argv, **kwargs)`. This function creates a normal derivation. Derivations are built by running the specified command with the specified environment variables, which can be specified using keyword properties of argv. The hash of a normal derivation is based on the provided argv and hashes of its dependencies. The return value is the path to the build output.
* `source(path)`. This function creates a source derivation. Source derivations have a fixed path, their hash is based on the file contents. As with `import`, the file path is relative to the caller. The return value is the path to the build output.
* `path(path)`. This function resolves the given relative path to an absolute path, in the same way as `source(path)`, however it does not try to read the file and its result is a normal string.
