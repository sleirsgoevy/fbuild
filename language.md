
# Type system

Types:

* int (the C int type)
* str (immutable string)
* dict (no special traits)
* function (can be called)

There are more types internally, but all of them mimic one of these when examined.

**Subject to change:** any object can have properties, indexed by either an integer or a string. In the future, only dict and function objects will be able to hold properties.

**Change possible:** there is no dedicated bool type. Boolean values are represented with integers 0 and 1; any nonzero integer is considered true. Values of other types cannot be used in boolean expressions.

# Functions

Functions can accept positional and keyword arguments. Positional arguments can only be passed by index, and keyword arguments can only be passed by keyword. In addition to the arguments, functions inherit the lexical scope they are called from.

# Syntax

The following constructs are available (ordered from lowest priority to highest priority):

## let..in

`let variable = expression; ...; in expr`

A recursive scope is constructed from variables in the left part of the expression. This scope is combined with the outer scope to evaluate the right part of the expression.

## Function definition

`(name[=default] OR *, ...) => body OR (**name) => body`

Argument names are specified in parentheses as a comma-separated list. Each argument can be given a default value via =. If instead of an argument * is specified, it separates positional arguments (to the left) and keyword arguments (to the right), otherwise all arguments are considered positional. The asterisk can appear at most once.

Default values of arguments can reference default values of other arguments in the same function definition, regardless of their order. Definitions of default values are lazy, values that are never used are not computed.

Alternatively a function definition can contain a single identifier prefixed with \*\*. When such a function is called, this argument will be set to a dict whose numeric keys contain positional arguments and string keys contain keyword arguments of the function call; no checks on arguments is performed in this case.

## Ternary operator

`cond ? iftrue : orelse`

The ternary operator specifies an if-then-else clause. If cond is true (nonzero integer), the expression evaluates to iftrue; otherwise it evaluates to orelse.

## Binary operators

Most binary operator are left-associative arithmetic operators. The table of all binary operators (with priorities) is given below, from lowest to highest. Division and modulo follow C language semantics.

* || (logical or, see below)
* && (logical and, see below)
* ==, !=, >=, <=, >, < (comparison operators, see below)
* | (bit or), // (dict overlaying, see below)
* ^ (bit xor)
* & (bit and)
* << (arithmetic shift left), >> (arithmetic shift right)
* +, -, ++ (array concatenation, see below), .. (string concatenation, see below)
* \*, / (integer devision), % (modulo)
* \*\* (power operator, right-associative)

## Logical operators

&& and || compute the logical AND or OR of the provided boolean values. If the left sub-expression defines the result unambigously, the right sub-expression is not evaluated. The value of the whole expression is always 0 or 1 without properties on them.

## Comparisons

Comparison operators operate on either ints or strings. If both arguments are ints, integer comparison is performed. Otherwise an attempt is made to coerce both arguments to strings and perform string comparison (lexicographical).

## Dict overlaying

The // operator can be used to create a modified version of a dict, and is equivalent to Python's dict.update. If the object on the left side is a function, the result has type function and can be called; the function does not see modified properties. Otherwise the result has type dict and special features of operands are discarded.

## Unary operations

There are 4 unary operations, which are executed right-to-left:

* +, -, ~ (bit not)
* \# (length). For strings this evaluates to the string length in bytes, for other objects this evaluates to the MEX of the object's integer keys.
* ' (quotation, see below)

Function calls, subscripts, and property access via a dot are essentially postfix unary operations; they are executed left-to-right without considering their priority.

`f(arg1, ..., argn, kw1=val, ..., kwn=val[, **val])`

Function arguments are specified in parentheses as a comma-separated list, similar to the declaration. Positional arguments are specified as expressions, and keyword arguments are specified as key-value pairs, where key must be an identifier. Positional arguments cannot follow keyboard arguments.

There can be at most one glob argument prefixed with \*\*. If it is specified, integer properties from the provided object will be passed as positional arguments, and string properties will be passed as keyword arguments. Explicitly passed arguments take precedence over values from the glob argument.

If a mandatory argument is omitted, too many positional arguments are supplied, or an unknown keyword argument is supplied, an error is reported.

A function call is a lazy operation, its value is computed only if it gets examined.

`d[idx]`

A subscript is specified by writing the index of the element to fetch in square brackets after the base expression. This operator fetches a property denoted by idx from the object denoted by d; idx must be either an int or a string. If there is no property with the given index, an error is reported.

`d.prop`

The dot operator is equivalent to the subscript operator, except that the property to be indexed is a compile-time constant and is specified as an identifier. Integer properties cannot be accessed this way.

## Array literal

`[value1, ..., valuen]`

This expression evaluates to a dict that has its integer properties 0 to n-1 assigned with values from the comma-separated list. This is a lazy operation, values that are never used are not computed.

## Dict literal

`{ key1 = value1; key2 = value2; ...; keyn = valuen; }`

This expression evaluates to a dict that has its string properties key1 to keyn assigned with values from the semicolon-separated list. The final semicolon must be specified if and only if the list is not empty. Keys must be identifiers, and are thus compile-time constants.

Sub-expressions being assigned to different keys can reference values being assigned to other keys by key names, regardless of the declaration order.

This is a lazy operation, values that are never used are not computed.

## Quotation

If the dict literal is prepended by a quote ('), for each element a function is emitted instead of a value. These functions do not use the normal dict literal recursion, and instead use the provided keyword arguments as the context. This operation is not lazy.

If other expression (including a dict literal enclosed in parentheses) is prepended by a quote ('), it is treated as a dict; a new dict is created that replaces each key's value with a function accepting arbitrary arguments and returning that value. This operation is lazy, values that are never used are not computed. This operation breaks intra-object recursion and thus is not equivalent to the dict quote operation above.

## Dict calling

If a dict is called, the result is an object whose properties are the call results of the function properties of the original dict, with the keyword arguments being lazily derived from the result itself. This operation is lazy, properties that are never accessed are not computed. This means that `('{...})()` is the same as `{...}`, and has the same lazily-recursive semantics.

## Parentheses

`(expr)`

This syntactical construct is semantically equivalent to just expr, but can be used to nest lower-priority operators inside higher-priority ones.

## String literals

`"string"`

Escape codes: \\n means newline, \\t means tabulation, \$ encodes a substitution. \\ with any other character means that character as is.
